import React, { Component } from 'react';

/*
  React.cloneElement(
    element,
    [props],
    [...children]
  )
*/

export class Toggler extends Component {
  render(){
    let { label, children, value, changeStatus, name } = this.props;
    return(
      <div>
        {label}
        <div className="togglerContainer">
          {
            //https://reactjs.org/docs/react-api.html#reactchildren
            React.Children.map( children, ( childItem ) => {
              if( React.isValidElement(childItem) ){
                // console.log( childItem );
                return React.cloneElement( childItem, {
                  parentName: name,
                  hanlder: changeStatus,
                  active: childItem.props.value === value 
                });
              } else {
                return null;
              }
            })
          }
        </div>
      </div>
    );
  }
}

export const TogglerItem = ({name, value, parentName, active, hanlder}) => {
  return(
    <div className={
      active === true ?
        "togglerItem active":
        "togglerItem"
      }
         data-value={value}
      onClick={
        hanlder !== undefined ?
          hanlder( parentName, value ) :
          null
      }
      >
      {name || value}
    </div>
  );
};
