import React, { Component } from 'react';


export default class InputContainer extends Component {

    state = {
        value: '',
        type: '',
        placeholder: 'this is a placeholder',

    }

    handler = (e) => {
        const value = e.target.value
        const type = e.target.type
        const placeholder = e.target.placeholder
        console.log(value)
        this.setState({
            value: value,
            type: type,
            placeholder: placeholder,

        })
    }


    render = () => {
        const {value, type, placeholder} = this.state;
        const {handler} = this;

        return (
            <div className="InputContainer">
                <label>
                    <div>My Input</div>
                    <input
                        type={type}
                        placeholder={placeholder}
                        value={value}
                        onChange={handler}
                    />
                </label>
            </div>
        );
    }

}


