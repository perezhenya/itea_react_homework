import React, { Component } from 'react'
import { Toggler, TogglerItem } from '../2_AdvancedChild/toggler';


export default class Form extends Component {
    state = {
        first: '',
        second: '',
        status: 'unactive',
        pop: 'true'
    }

    handleInput = ( e ) => {
        const name = e.target.name;
        const value = e.target.value;
        console.log(name, value);
        this.setState({
            [name]: value
        });
    }

    changeToggler = ( value ) => (e) => {
        console.log('status:', e.target, value );

        this.setState({
            status: value
        });
    }

    handleSubmit = (e) => {
        e.preventDefault();
        console.log( this.state );
    }

    render() {
        const { handleInput, handleSubmit, changeToggler} = this;
        const { first, second, status } = this.state;
        return (
            <form onSubmit={handleSubmit}>
                <input 
                    onChange={handleInput}
                    name="first" 
                    value={first}
                />
                <input 
                    onChange={handleInput}
                    name="second" 
                    value={second}
                />
                <Toggler
                    activeToggler={status}
                    changeStatus={changeToggler}
                >
                    <TogglerItem value="active"> Active  </TogglerItem>
                    <TogglerItem value="unactive"> Unactive  </TogglerItem>
                </Toggler>
                <button> Send </button>
            </form>
        )
    }
}
