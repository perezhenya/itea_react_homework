import React from 'react';


const Guest = ({guest, onClick}) => {

    return (
        <div className="guest">
            <div className="guest-body">
                <div className="row">
                <div className="col-9">
                <div className="d-flex justify-content-between align-items-center">
                    <ul>
                        <li key={guest.index}>
                            <h3>{guest.name}</h3>
                            <h5>{guest.company}</h5>
                            <h6>{guest.phone}</h6>
                            <h6>{guest.address}</h6>
                            <h6>{guest.about}</h6>
                        </li>

                    </ul>
                    <button
                        type="button"
                        className="btn btn-secondary"
                        onClick={onClick}
                        data-index={guest.index}
                    >
                        Прибыл
                    </button>
                </div>
                </div>
            </div>
            </div>

        </div>
    )
}

export default Guest;