import React from 'react';
import Guest from "./Guest";


const GuestsList = ({guests,filteredGuests,handler}) => {

    // let guestSearch = guests.filter(guest =>
    //     guest.name.toLowerCase()
    //         .indexOf(filteredGuests.toLowerCase()) !== -1);
    let guestSearch = guests.filter(o => {
       return Object.keys(o).some(k => {
          return  o[k].toString().toLowerCase().indexOf(filteredGuests.toLowerCase()) !== -1;
        })})


    const loaded = true;
    return (
        <div >
            {
                loaded && (
                    <ul>
                        {
                            guestSearch.map((guest, key) =>
                                <Guest
                                    key={key}
                                    guest={guest}
                                    onClick = {handler}
                                />
                            )}

                    </ul>
                )
            }

        </div>
    )
}

export default GuestsList;

