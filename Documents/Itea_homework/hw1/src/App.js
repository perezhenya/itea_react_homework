import React, {Component} from 'react';
import GuestsList from './Guests';
import classWorkGuests from './classWork_guests.json';


class App extends Component {

  state = {
    guests: classWorkGuests,
    filteredGuests: '',
    presentGuests: [],
  }

  handleInput = e => {
    this.setState({
      filteredGuests: e.target.value,
    })
  }

    deleteGuest = (e ) => {
        const id = Number(e.target.dataset.index);
        let changedGuests = this.state.guests.filter( item => item.index !== id)

        this.setState({
            guests: changedGuests
        });

    }


    render = () => {
    const {
      guests,
      filteredGuests
    } = this.state

    const {
      handleInput,
      deleteGuest,
    } = this

    return(
       <div className="container">
           <div className="row">
               <div className="col-10 mb-4">
           <h2 style={{textAlign: "center"}}>
               Список гостей
           </h2>
           <input
               style={{margin: "0 auto",display: "block", marginBottom: "40px"}}
               type="text"
               placeholder="Type a guest..."
               onChange={handleInput}

           />
           <GuestsList
               guests={guests}
               filteredGuests={filteredGuests}
               handler={deleteGuest}
           />
       </div>

               </div>
           </div>


    )
  }
}

export default App;
