const initialState = {
    data: [
        {
            id: '1',
            title:"hello world",
        }
    ],
    completed: false

}

const reducer = (state = initialState, action) => {
    switch(action.type){
        case 'ADD_TODO':
            return {
                ...state,
                data: [...state.data, action.payload],
                completed: true

            }

        case 'REMOVE_TODO':
            return {
                ...state,
                data: [...state.data.filter(dataItem => dataItem.id !== state.data.id)]
            }
        default:
            return state;
    }
}




export default reducer;