import React from 'react';
import {connect} from 'react-redux';
import '../App.css';



class List extends React.Component {

    state = {
        newToDo: [],
    }

    changeValue = (e) => {
        this.setState({
            newToDo: e.target.value
        });
    }

    addToList = () => {
        const {addToDo} = this.props;
        addToDo(this.state.newToDo)
    }


    render = () => {
        const {newToDo} = this.state;
        const {changeValue,addToList}  = this

    return(
        <div className="input-container">
            <input className="input-item"
                   value={newToDo}
                   onChange={changeValue}
            />
            <button onClick={addToList}>Add Todo</button>

        </div>
    )

    }
}
//
const mapStateToProps = state => {
    return {
        list: state.data
    }
}

const mapDispatchToProps = dispatch => ({
    addToDo: (title) => {
        dispatch({
            type: 'ADD_TODO',
            payload: {
                title: title
            }
        })
    },
    // toggleTodo: (id) => {
    //     dispatch({
    //         type: 'REMOVE_TODO',
    //         deletedData: {
    //             id: id
    //         }
    //
    //     })
    // }
})

export default connect(mapStateToProps, mapDispatchToProps)(List);