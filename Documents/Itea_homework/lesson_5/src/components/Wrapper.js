import React from 'react';
import {Provider} from 'react-redux';
import store from '../store/index';
import List from '../components/List';
import {BrowserRouter, Route, Link, Switch} from 'react-router-dom'
import All from '../components/All';
import Completed from '../components/Completed';

function Wrapper() {
    return (
      <Provider store={store}>
         <BrowserRouter>
             <div>
                 <header>
                     <ul>
                         <li><Link to='/all'>All</Link></li>
                         <li><Link to='/uncompleted'>uncompleted</Link></li>
                         <li><Link to='/completed'>completed</Link></li>
                         <li><Link to='/list'>list</Link></li>
                     </ul>
                 </header>
             </div>
             <Switch>
                 <Route exact path='/all' children={<All/>}/>
                 <Route exact path='/uncompleted' />
                 <Route exact path='/completed' children={<Completed/>}/>
                 <Route exact path='/list' children={<List/>}/>

             </Switch>

         </BrowserRouter>
      </Provider>

    );
}

export default Wrapper;