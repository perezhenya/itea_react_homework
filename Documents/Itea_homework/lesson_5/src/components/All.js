import React from 'react';
import {connect} from "react-redux";


const All = ({list, toggleTodo}) => {

   const removeFromList = (id )  => {
        toggleTodo(id)
    }
    return (
      <div>
          <ul>
              {
                  list.map((item,key) => (
                      <li key={key}>
                          {item.title}
                          <button onClick={removeFromList}>completed</button>
                      </li>
                  ))
              }

          </ul>

      </div>

    );
}

const mapStateToProps = state => {
    return {
        list: state.data
    }
}
const mapDispatchToProps = dispatch => ({
    toggleTodo: (id) => {
        dispatch({
            type: 'REMOVE_TODO',
            deletedData: {
                id: id
            }

        })
    }
})

export default connect(mapStateToProps,mapDispatchToProps)(All);