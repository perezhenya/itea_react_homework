import React from 'react';
import {connect} from "react-redux";


const Completed = ({list}) => {

        return (

            <div>
                <ul>
                    {
                        list.map((item, key) => (
                            <li key={key}>{item.title}
                            </li>

                        ))
                    }

                </ul>

            </div>

        );

}

const mapStateToProps = state => {
        return {
            list: state.data,
            completed: state.completed
        }
    }



export default connect(mapStateToProps,null)(Completed);