/*

    Задание 2.

    -> Breadcrumbs

    Написать функциональный компонент, который будет генерировать хлебные крошки 
    исходя из переданых в него данных.


    <Breadcrumbs 
        items={[
            {
                path: '/#/',
                name: 'Main'
            },
            {
                path: '/#/category
                name: 'All categories'
            },
            {
                path: '/#/category/1,
                name: 'Cats and dogs accessories'
            }
        ]}
    />


*/