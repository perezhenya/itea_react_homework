import React from "react"
import './App.css';
import Button from './components/Button';



class App extends React.Component{

	state = {
		style: {
			backgroundColor: "white",
		}
	}
	doSomething = () => {
		// console.log('Button pressed');
		this.setState({
			style: {
				...this.state.style,//взять старое и перезаписать
				backgroundColor: "red"
			}
		});
	}

	render = () => {
		const {style} = this.state
		const {doSomething} = this

		return (
			<div className="App">
			<Button
				text="Hello world"
				handler={doSomething}
				style={style}
				 />

			</div>
		);
	}
}

export default App;
