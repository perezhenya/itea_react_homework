import React, { Component } from 'react'


class ErrorBoundary extends Component{

    state = {
        hasError: false
    }

    componentDidCatch( error, info ){
        console.log(error, info );
        this.setState({
            hasError: true
        })

    }
    
    static getDerivedStateFromError(error) {
        // Обновление состояния, чтобы при последующей отрисовке показать аварийный UI.
        return { hasError: true };
    }


    render = () => {
        if( this.state.hasError){
            return(
                <div> Opps! </div>
            )
        } 
        return this.props.children;
    }
}

export default ErrorBoundary;






