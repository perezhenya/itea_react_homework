import React from 'react'

const Button = ({ handler, style, text, children }) => {
    console.log( style, text, children  )
    return(
        <button 
            style={style}
            onClick={handler}>
            {text !== null ? text : children}
        </button>
    )
}

Button.defaultProps = {
    text: null,
    style: {
        backgroundColor: 'blue',
        color: '#fff'
    },
    handler: () => { console.log('default handler')}
}

export default Button;
