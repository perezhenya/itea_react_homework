import React, { Component } from 'react'

import Button from './button';

import ChildContainer from '../childs';

class FormComponent extends Component {
    
    static defaultProps = {
        test: true
    }

    state = {
        hello: true,
        style: {
            color: 'red',
            background: 'yellow'
        }
    }

    clickHandler = () => {
        this.setState({
            hello: !this.state.hello,
            style: {
                ...this.state.style,
                color: '#e1e1e1'
            }
        })
    }

    render = () => {
        const { clickHandler } = this;
        const { hello, style } = this.state;

        return(
            <div className="container">
                <ChildContainer type="main">
                    <h1> Title</h1>
                </ChildContainer>
                <ChildContainer type="aside">
                    <Button
                        style={style}
                        handler={clickHandler}
                    >
                        Sumbit as child
                    </Button>
                </ChildContainer>
            </div>
        )
    }
}


export default FormComponent;