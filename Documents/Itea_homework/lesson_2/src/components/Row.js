import React from 'react';
import '../App.css';

const Row = ({type, children}) => {
    Row.defaultProps = {
        head: "false",
    }
    return (
        <div className={type}>
            {/*<tr>*/}
            {/*    <td>1</td>*/}
            {/*    <td>2</td>*/}
            {/*    <td>3</td>*/}
            {/*    <td>4</td>*/}
            {/*</tr>*/}

            {children}
        </div>
    )
}

export default Row;