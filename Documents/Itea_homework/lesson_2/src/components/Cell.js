import React from 'react';
import '../App.css';

const Cell = ({type, cells,style}) => {
    Cell.defaultProps = {
        type: 'text',
        cells: 1,
        background: 'transparent',
        color: 'black',
    }


    if( type === ' money'){
        return(
            <div>
                ...
            </div>
        )
    }

    return (
  <div>
      <tr>
          <td style={style}>1</td>
          <td >2</td>
          <td>3</td>
          <td>4</td>
      </tr>
      <tr>
          <td >1</td>
          <td >2</td>
          <td>3</td>
          <td>4</td>
      </tr>
      <tr>
          <td >1</td>
          <td >2</td>
          <td>3</td>
          <td>4</td>
      </tr>
      <tr>
          <td style={style}>1</td>
          <td >2</td>
          <td>3</td>
          <td>4</td>
      </tr>
  </div>

    )
}

export default Cell;