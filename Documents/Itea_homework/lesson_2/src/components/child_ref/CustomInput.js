import React, { Component } from 'react';
import './input.css';

const CustomInput = ({ label, name, value, limit, changeHandler }) => (
    <label className="custom-input">
        <span>{label}</span>
        <input 
            maxLength={ limit !== undefined ? limit : null}
            name={name}
            type="text" 
            value={value} 
            onChange={changeHandler} 
        />
    </label>
);

export default CustomInput