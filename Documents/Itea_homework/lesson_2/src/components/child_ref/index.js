import React, { Component } from 'react';
import Buttonz from './Button';
import ChildDemo from './Children';

import CustomInput from './CustomInput';

const style = {
  color: 'palevioletred',
  backgroundColor: 'papayawhip',
  padding: '5px 20px'
}

class ChildElements extends Component {
  state = {
    show: true,
    btnMsg: 'Empty state',
    course: '',
    text: '',
    data: [
      {id: 0, status: false},
      {id: 1, status: false},
      {id: 2, status: false}
    ]
  }

  changeData = ( id ) => ( e ) => {
    let newData = [...this.state.data].map( item => {
      if( item.id === id ){
        item.status = true;
      }
      return item;
    });

    this.setState({
      data: newData
    })
  }

  updateProps = (event) => {
    console.log('Update props of Lifecycle method');
    this.setState({btnMsg: 123});
  };

  changeHandler = (e) => {
      let value = e.target.value;
      let name = e.target.name;
      console.log( name, value);
      this.setState({
        [name]: value
      })
  }

  render = () => {
    let { course, text, btnMsg, show, data } = this.state;
    let { changeHandler, updateProps, changeShow } = this;
    return(
      <div>
        {
          show && (
            <Buttonz
              action={this.changeData(1)}
            />
          )
        }
        <ul>
          {
            data.map( guest => (<li>{`${guest.id} ${guest.status}`}</li>))
          }
        </ul>
        
      </div>
    );
  }
}




export default ChildElements;
