import React from "react";

const Button = ({handler, style}) => (

    <button onClick={handler}
            style={style}
   >
        Push the Button
    </button>


)

export default Button;