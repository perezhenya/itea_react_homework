import React from 'react';


const List = ({users, handler, style}) => {
    return (

        <div className="d-flex justify-content-between align-items-center">
            <ul>
                {
                    users.map((user, key)=>
                        <li key={key}>
                            <h1>{user.name}</h1>
                            <h3>{user.email}</h3>
                            <h4>{user.phone}</h4>
                        </li>

                    )
                }
            </ul>
            <button
                type="button"
                className="btn btn-secondary"
                onClick={handler}
                style={style}
            >
                Push the Button
            </button>
        </div>





    )
}

export default List;