import React, { Component } from 'react';
import List from '../components/List';
// import Button from '../components/Button';



class ListContainer extends Component {

    state = {
        users: [],
        interviewed: true,
        style: {
            backgroundColor: "green",
        }
    }

    notInterviewed = () => {
        if (!this.state.interviewed) {
            this.setState({
                interviewed: false,
                style: {
                    ...this.state.style,
                    backgroundColor: "red",
                }
            });
        }
    }


        componentDidMount()
        {
            fetch('http://www.json-generator.com/api/json/get/craSVzeffS?indent=2')
                .then(res => res.json())
                .then(users =>
                    this.setState({
                        users: users,
                        interviewed: false,
                    })
                )

        }


        render = () => {
            const {users, style} = this.state
            const {notInterviewed} = this

            return (
                <div className="ListContainer">
                    <List users={users}
                          text="Hello world"
                          handler={notInterviewed}
                          style={style}/>

                </div>
            );
        }

}

export default ListContainer;