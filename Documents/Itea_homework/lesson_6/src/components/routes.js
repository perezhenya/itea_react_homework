import Authentication from '../components/Authentication';
import Login from '../components/Login';
import LoggedUsers from '../components/LoggedUsers';

export default [
    {
        path: '/',
        exact: true,
        component:Authentication,
    },
    {
        path: '/login',
        exact: true,
        component:Login,

    },
    {
        path: '/',
        exact: true,
        component:LoggedUsers,
        private: true,

    }
]