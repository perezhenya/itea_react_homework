import React from 'react';
import '../App.css';
import {AuthContext} from "./AuthContext";


class Authentication extends React.Component {

    static contextType = AuthContext;
    state = {
        email: '',
        password: '',
    }

    handleChange = (e) => {
        this.setState({
            [e.target.name] : e.target.value
        })
    }

    onSubmit = (e) => {
        e.preventDefault();
        fetch('http//http://172.17.13.189:4004/auth/registration',
            {
                method: 'POST',
                headers: {
                    "content-type": "application/json",
                },
                body: JSON.stringify({
                    email: 'zhenya@gmail.com',
                    password: '12345678',
                    }
                )
            })
            .then(res => res.json())
    }

    render = () => {
        return(
            <div className="form-login-container">
                <form className="form-login" onSubmit={this.onSubmit}>
                    <h1 className="h3 mb-3 font-weight-normal text-center">
                        Authorization
                    </h1>
                    <div className="form-group">
                        <label htmlFor="username">Email</label>
                        <input
                            type="text"
                            className="form-control"
                            placeholder="Пользователь"
                            name="email"
                            value={this.state.email}
                            onChange={this.handleChange}
                        />
                    </div>
                    <div className="form-group">
                        <label htmlFor="password">Password</label>
                        <input
                            type="password"
                            className="form-control"
                            placeholder="Пароль"
                            name="password"
                            value={this.state.password}
                            onChange={this.handleChange}
                        />
                    </div>
                    <button type="submit" className="btn btn-lg btn-primary btn-block">
                        Вход
                    </button>
                </form>

            </div>








        )
    }
}
export default Authentication;