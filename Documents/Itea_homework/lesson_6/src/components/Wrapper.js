import React from 'react';
import {BrowserRouter, Link, Switch, Route} from 'react-router-dom';
import routes from '../components/routes';
import PrivateRoute from '../components/PrivateRoute';
import AuthContext from '../components/AuthContext';

 const Wrapper = () => {
     return (
         <BrowserRouter>
             <AuthContext>
             <div>
                 <header>
                     <ul>
                         <li><Link to='/'>Authorization</Link></li>
                         <li><Link to='/login'>Login</Link></li>
                         <li><Link to='/list'>LoggedPeople</Link></li>

                     </ul>
                 </header>
                 <Switch>
                     {
                         routes.map((route, key) => {
                             if (!route.private) {
                                 return <Route key={key} {...route}/>
                             } else {
                                 return <PrivateRoute key={key} {...route}/>
                             }
                         })
                     }
                 </Switch>
             </div>
             </AuthContext>
         </BrowserRouter>

     )
 }

export default Wrapper;