import React, { Component } from 'react'


const TitleContext = React.createContext();
const ThemeContext = React.createContext();


export const LevelZero = () => ( <LevelOne /> );
export const LevelOne = () => ( <LevelTwo /> );
export const LevelTwo = () => ( <LevelThree /> );
export const LevelThree = () => (
    <ThemeContext.Consumer>
        {
            value => {
                console.log('context', value );
                return (
                    <section>
                        <h2> Current theme {value.theme} </h2>
                        <button
                            className={`button ${value.theme}`}
                            onClick={ value.changeTheme('red') }
                        >Red</button>
                        <button
                            className={`button ${value.theme}`}
                            onClick={ value.changeTheme('blue') }
                        >Blue</button>
                        <button
                            className={`button ${value.theme}`}
                            onClick={ value.changeTheme('yellow') }
                        >Yellow</button>
                    </section>
                )
            }
        }
    </ThemeContext.Consumer>
)

export default class ContextComponent extends Component {

    state = {
        theme: 'red'
    }

    changeTheme = ( theme ) => (e) => {
        this.setState({ theme });
    }

    render() {
        const { theme } = this.state;
        const { changeTheme } = this;
        return (
            <div>
                <ThemeContext.Provider 
                    value={{
                        theme,
                        changeTheme
                    }}>
                    <LevelZero/>
                </ThemeContext.Provider>
            </div>
        )
    }
}
