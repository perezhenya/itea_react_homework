import React, { Component } from 'react';
import HomeToggler from "./HomeToggler";
import TabItem from '../components/TabItem';
import './Toggler.css';


export default class HomeTogglerContainer extends Component {

    state = {
        activeTab: "left",
        gender: "male",

    }

    changeStatus = (e) => {
        const togglerValue = e.target.dataset.value;
        this.setState({
            activeTab: togglerValue,
            gender: "female",
        })
    }

    render(){
        const {activeTab, gender} = this.state;
        const {changeStatus} = this;

            return(
                <div>
                    <div className="togglerContainer">
                        <HomeToggler activeTab={activeTab}
                                     changeStatus={changeStatus}
                                     gender={gender}>
                            <TabItem value="left" name="left"/>
                            <TabItem value="right" name="right"/>
                        </HomeToggler>

                    </div>
                </div>
        );
    }
}