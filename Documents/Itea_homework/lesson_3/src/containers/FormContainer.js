import React, { Component } from 'react';
import HomeToggler from './HomeToggler';
import TabItem from '../components/TabItem';
import './Toggler.css';

export default class FormContainer extends Component {

    state = {
        first: '',
        second:'',
        type: "password",
        status: "male",

    }

    handler = (e) => {
        const value = e.target.value;
        const name = e.target.name;
        console.log(value)
        this.setState({
            [name] : value,  //?
        })
    }

    typeHandler = () => {
        if (this.state.type !== Number('123')) {
            alert ('Password, please')
        }
    }
    changeTab = (e) => {
        console.log(e)
        const value = e.target.dataset.value;
        this.setState({
            status: value,

        })
    }

    handleSubmit = (e) => {
        e.preventDefault();
        console.log(this.state)
    }

    render = () => {
        const {first, second, type, status} = this.state;
        const {handler, typeHandler,changeTab, handleSubmit} = this;

        return (
            <div className="FormContainer">
                <form>
                    <div>My Form</div>
                    <input
                        placeholder="Type something"
                        value={first}
                        onChange={handler}
                        name="first"
                    />
                    <input
                        type={type}
                        placeholder="Type something"
                        value={second}
                        onChange={typeHandler}
                        name="second"
                    />
                    <HomeToggler
                        activeTab={status}
                        changeStatus={changeTab}
                    >
                        <TabItem value="male" name="male"/>
                        <TabItem value="female" name="female"/>

                    </HomeToggler>
                    <button
                        type="submit"
                        onSubmit={handleSubmit}>Button
                    </button>
                </form>
            </div>
        );
    }

}





