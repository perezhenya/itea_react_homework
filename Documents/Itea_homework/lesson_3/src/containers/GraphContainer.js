import React from 'react';
import Chart from 'chart.js';

 export default class GraphContainer extends React.Component {

    state = {
        data: [0, 10, 5, 2, 20, 30, 45],
    }

     chartEl  = React.createRef();

    componentDidMount() {
        this.chart =  new Chart(this.chartEl.current, {
            type: 'line',
            data: {
                labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
                datasets: [{
                    label: 'My First dataset',
                    backgroundColor: 'rgb(255, 99, 132)',
                    borderColor: 'rgb(255, 99, 132)',
                    data: this.state.data,
                }],

            },
            options: {},
        });

    }

    componentDidUpdate(prevProps, prevState) {
        this.chart.data.datasets[0].data = prevState.data;
        this.chart.update();
    }

    randomizeData = (min, max) => (e)  => {
        e.preventDefault();
        let data = this.chart.data.datasets[0].data;
        let updatedNums = data.map( item => Math.floor(Math.random() * (max - min)) + min );
        console.log( updatedNums )
        this.setState({
            data: updatedNums
        })
     }

    render = () => {
        const {randomizeData} = this;
        return(
           <div>
               <canvas ref={this.chartEl}/>
               <button onClick={randomizeData(0, 60)}>Randomize Data</button>
           </div>



        )
    }
}