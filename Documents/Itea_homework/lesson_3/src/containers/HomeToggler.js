import React, { Component } from 'react';
import PropTypes from 'prop-types';

/*
  React.cloneElement(
    element,
    [props],
    [...children]
  )
*/

export default class HomeToggler extends Component {

    render(){
        let {children, changeStatus, activeTab, gender} = this.props;

        return(
            <div>
                <div className="togglerContainer">
                    {
                        React.Children.map(
                            children,
                            ChildrenItem => {
                                if (React.isValidElement(ChildrenItem)) {
                                    console.log("true")
                                } else {
                                    console.log("false")
                                }
                                    return React.cloneElement(
                                        ChildrenItem, {
                                        ...ChildrenItem.props.value,
                                        activeTab: ChildrenItem.props.value === activeTab ? true : false,
                                        changeStatus: changeStatus,
                                        gender: ChildrenItem.props.value === activeTab ? "female" : "male",

                                        }
                                    )

                                }
                        )
                    }

                </div>
            </div>
        );
    }
}

HomeToggler.propTypes = {
    children: PropTypes.array,
    activeTab: PropTypes.string,
    gender: PropTypes.string,
    changeStatus: PropTypes.func.isRequired,



}
