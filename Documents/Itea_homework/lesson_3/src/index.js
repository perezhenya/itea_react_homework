import React from 'react';
import ReactDOM from 'react-dom';
import './style/index.css';

import App from './App';
// import InputContainer from './containers/InputContainer';
import FormContainer from './containers/FormContainer';
import GraphContainer from './containers/GraphContainer';
import registerServiceWorker from './registerServiceWorker';
import HomeTogglerContainer from './containers/HomeTogglerContainer';

ReactDOM.render( <HomeTogglerContainer/>, document.getElementById('root'));
registerServiceWorker();
 {/*<FormContainer/> <HomeTogglerContainer/> <InputContainer/> <GraphContainer/>*/}