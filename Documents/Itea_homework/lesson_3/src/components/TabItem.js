import React from "react";
import PropTypes from 'prop-types';

 const TabItem = ({value, activeTab,changeStatus, name, gender}) => {
    return(
        <div
            className={
                activeTab === true ?
                "togglerItem active":
                    "togglerItem"
            }

            onClick = {(value) => changeStatus(value)}
            data-value={value}
            gender={gender}
        >
            {name && value && gender}
        </div>




    )
}

export default TabItem;

TabItem.propTypes = {
    name: PropTypes.string.isRequired,
    activeTab:PropTypes.bool,
    changeStatus:PropTypes.func,
    gender: PropTypes.string

}