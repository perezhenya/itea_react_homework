import React, { Component } from 'react';

class DemoRef extends Component{

  inputRef = React.createRef();

  handler = (e) => {
    console.log(e.target);
  }

  componentDidMount = () => {


    const html = `<div><h1>Hello</h1><ul><li>1</li><li>2</li></ul></div>`;
    this.inputRef.current.innerHTML = html;
    // console.log( 'this._myRef', this._myRef );
    // console.log( 'this.inputRef.current', this.inputRef.current );
    //
    // this._myRef.style.background = "red";
    // this._myRef.value = "123"
  }

  myRefMethod = (node) => {
    console.log('myRefMethod', node);
    node.style.background = "blue";
  }

  render = () => {
    let { handler } = this;
   
    return(
      <div ref={ this.inputRef }>
        {
          //ref={ this.inputRef }
          //ref={ node => this._myRef = node  }
          //ref={ node => this.myRefMethod( node ) }
          // <input
          //   ref={ node => this.myRefMethod( node )  }
          //   onChange={handler}
          //   placeholder="Text"
          //   />
        }
      </div>
    );
  }
}

export default DemoRef;
