import React, { Component } from 'react';
import './App.css';
import './style/App.css';

// import CompWithPropTypes from './1_PropTypes/';
// import AdvancedChild from './2_AdvancedChild';
import {Toggler, TogglerItem} from './2_AdvancedChild/toggler';
// import ControlledForm from './3_ControlledForm';
// import Ref from './Refs';

import ContextComponent from './5_Context';


// const ComponentToSend = () => (<div>Null</div>);

// import Chart from './chart';

const ComponentToSend = () => (<div>Null</div>);


class App extends Component {
  state = {
    textDemo: 'null',
    textDemo2: 'null2',
    activeToggler: "left",
    status: 'middle'
  }
  changeHandler = (e) => {
    let value = e.target.value;
    console.log('value:', value)
    this.setState({
      textDemo: value
    })
  }


  
  changeStatus = ( key, value ) => () => {
    this.setState({
        [key]: value
      });
  }

  render = () => {
    let { textDemo,  textDemo2, activeToggler, status } = this.state;
    let { changeHandler } = this;
    return(
      <div className="wrap">
        <h1> Lesson 3: PropTypes, ControlledComponents, AdvancedChild Methods, Refs</h1>
        {/* 
            
            
            
            

           */}
        {
          // Demo 1 -> PropTypes
          // <CompWithPropTypes

          //   somethingTrue={true}
          //   stringProp="String"
          //   arrayProp={[]}

          //   action={ () => console.log(123)}
          //   type="Bar"

          //   ReactEl={<ComponentToSend/>}

          //   user ={{
          //     name: 'Test',
          //     count: 10
          //   }} 
          //   someStuff={<ComponentToSend/>}
          // >
          //   <ComponentToSend />
          // </CompWithPropTypes>
        }


          {/*// Demo 2: AdvancedChild*/}
        {/*// <AdvancedChild myProp="123">*/}
        {/*//   <ComponentToSend test="123"/>*/}
        {/*// </AdvancedChild>*/}
        }
          <Toggler
          label="Choose layout"
          activeToggler={activeToggler}
          changeStatus={this.changeStatus}>
          <TogglerItem value="left" label="Choose layout" />
          <TogglerItem name="middle" value="center"/>
          <TogglerItem value="right"/>
          </Toggler>

        {
          // Demo 2: AdvancedChild
          // <AdvancedChild myProp="123">
          //   <ComponentToSend test="123"/>
          // </AdvancedChild>
          // <Toggler
          //   name="activeToggler"
          //   label="Choose layout"
          //   value={activeToggler}
          //   changeStatus={this.changeStatus}
          // >
          //   <TogglerItem value="left" label="Choose layout" />
          //   <TogglerItem name="middle" value="center"/>
          //   <TogglerItem value="right"/>
          //   123
          // </Toggler>

        }
        {/* <Toggler
            name="status"
            label="Choose layout"
            value={status}
            changeStatus={this.changeStatus}
          >
            <TogglerItem value="left" label="Choose layout" />
            <TogglerItem name="middle" value="center"/>
            <TogglerItem value="right"/>
            123
          </Toggler> */}
        {
          // Demo 3
          // <Ref/>
        }

        {
          // Demo 4 - controlled form
          // <ControlledForm />
        }

        {
          // Demo 5 - contxt
          // <ContextComponent />
        }

        {/*<Chart />*/}
      </div>
    );
  }
}

export default App;
