import {combineReducers} from 'redux';
import postsReducer from "../reducers/postsReducer";
// import postItemReducer from "../reducers/postItemReducer";



const reducer = combineReducers({
    posts:postsReducer,
    // item:postItemReducer,
})

export default reducer;