import {
    GET_POST_REQ,
    GET_POST_RES,
    GET_POST_ERR,
} from '../actions/postsAction';

const postsInitialState =  {
    data: [],
    errors: [],
    loaded: false,

};

const postsReducer = (state = postsInitialState, action) => {
  switch (action.type) {
      case GET_POST_REQ:
          return {
              ...state,
              loaded: false,
          };
      case GET_POST_RES:
          return {
              ...state,
              loaded: true,
              data: action.payload
          };
      case GET_POST_ERR:
          return {
              ...state,
              errors: action.payload
          };
      default:
          return state;
}
}

export default postsReducer;