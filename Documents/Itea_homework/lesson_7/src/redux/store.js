import {createStore, compose, applyMiddleware} from 'redux';
import reducer from '../reducers/reducer';
import {loadState, saveState} from './preloaded';
import thunk from 'redux-thunk';

const composeEnhancers = process.env.NODE_ENV !== 'production' && typeof window !== 'undefined' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__  ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ : compose;

const middleware = applyMiddleware(
    thunk,
)

let store;
const preloadedState = loadState();
if (preloadedState === null) {
 store = createStore(reducer, composeEnhancers(middleware) );
} else {
  store = createStore(reducer,preloadedState, composeEnhancers(middleware) );
}

store.subscribe(() => {
 const state = store.getState();
 let data = state.posts.data;
 saveState({
  data
 })
})



export default store;