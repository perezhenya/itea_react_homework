import React from 'react';
import {getPosts, getLimitedPosts} from '../actions';
import {connect} from 'react-redux';
import Helmet from 'react-helmet';
import {Link} from 'react-router-dom';
import '../App.css';
import favicon from '../puzzle.png';

class AllPosts extends React.Component {

  componentDidMount() {
    this.props.handlePosts()
  }

  // getSomeLimitedPosts() {
  //     this.props.handleLimitedPosts()
  // }


  render = () => {
 const {items,handleLimitedPosts} = this.props;

    return (
        <>
            <Helmet>
                <meta charSet="utf-8" />
                <title>Main Page</title>
                <link rel="shortcut icon" href={favicon}/>
            </Helmet>
            <ol>
                {
                    items.map(item => (
                        <li key={item.id}>
                        <Link  to={`/users/${item.userId}`}>{item.userId}</Link>
                            <h3>{item.title}</h3>
                            <h5>{item.body}</h5>
                            <button><Link to={`/users/posts/${item.id}`}>Show the post</Link></button>
                        </li>
                        )
                    )
                }
                </ol>
            <button onClick={handleLimitedPosts}>Show more</button>

        </>

    );
  }
}

const mapStateToProps = (state) => ({
        items: state.posts.data,
        limitedItems: state.posts.data
});


const mapDispatchToProps = (dispatch) => ({
    handlePosts: () => {
      dispatch(getPosts())
    },
    handleLimitedPosts: () => {
        dispatch(getLimitedPosts())
    },

});



export default connect (mapStateToProps, mapDispatchToProps)(AllPosts);
