import React from 'react';
class AllUserPosts extends React.Component {

    state = {
        postItem: []
    }

    componentDidMount() {
        fetch(`http://jsonplaceholder.typicode.com/posts/${this.props.match.params.id}`)
            .then(response => response.json())
            .then(responseList => {
                this.setState({
                    postItem: responseList
                })
            })
    }


    render = () => {
        const {postItem} = this.state

        return (
            <>
                <ul>
                    {
                      Object.keys(postItem).map(key => (
                          <li key={key}>{postItem[key]}</li>
                      ))


                    }

                </ul>


            </>

        );
    }
}

export default AllUserPosts;
