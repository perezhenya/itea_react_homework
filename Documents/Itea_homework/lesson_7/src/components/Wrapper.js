import React from 'react';
import {Provider} from 'react-redux';
import store from '../redux/store';
// import AllPosts from '../components/AllPosts';
import {BrowserRouter,Route, Switch} from 'react-router-dom';
import PostsWrapper from '../components/PostsWrapper';
import AllPosts from "./AllPosts";


const Wrapper = () => (

  <Provider store={store}>
      <BrowserRouter>
          <Switch>
              <Route exact path='/' component={PostsWrapper}/>

          </Switch>

      </BrowserRouter>
  </Provider>


)




export default Wrapper;