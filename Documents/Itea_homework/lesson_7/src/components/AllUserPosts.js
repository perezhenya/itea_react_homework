import React from 'react';
import Helmet from 'react-helmet';

class AllUserPosts extends React.Component {

    state = {
        userPosts: []
    }

    componentDidMount() {
        fetch(`https://jsonplaceholder.typicode.com/posts?userId=${this.props.match.params.id}`)
            .then(response => response.json())
            .then(responseList => {
                this.setState({
                    userPosts: responseList
            })
            })
}


    render = () => {
        const {userPosts} = this.state


        return (
            <>
                <Helmet>
                    <meta charSet="utf-8" />
                    <title>All posts of user</title>
                </Helmet>
                <ul>
                    {
                        userPosts.map(item => (
                            <li key={item.id}>
                                <h5>{item.id}</h5>
                                <h4>{item.title}</h4>
                                <h5>{item.body}</h5>
                            </li>
                                )
                            )


                    }

                </ul>


            </>

        );
    }
}

export default AllUserPosts;
