import React from 'react';
import  {BrowserRouter, Switch, Route} from 'react-router-dom';
import AllPosts from '../components/AllPosts';
import AllUserPosts from './AllUserPosts';
import PostItem from '../components/PostItem';

const PostsWrapper = () => {

    return(
        <BrowserRouter>
            <Switch>
                <Route exact path='/' component={AllPosts}/>
                <Route exact path='/users/:id' component={AllUserPosts}/>
                <Route exact path='/users/posts/:id' component={PostItem}/>
            </Switch>
        </BrowserRouter>




    )






}

export default PostsWrapper;