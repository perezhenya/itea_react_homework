export const GET_POST_REQ = 'GET_POST_REQ';
export const GET_POST_RES = 'GET_POST_RES';
export const GET_POST_ERR = 'GET_POST_ERR';


export const getPosts = () => (dispatch) => {
    dispatch({
        type: GET_POST_REQ
    });
    let limit = 50;
    fetch('https://jsonplaceholder.typicode.com/posts/')
        .then(response => response.json())
        .then(res => res.slice(0, limit))
        .then(responseData => {
            dispatch({
                type: GET_POST_RES,
                payload: responseData,
            })
        })
        .catch(err => {
            dispatch({
                type: GET_POST_ERR,
                payload: err,
            })
        })
}

export const getLimitedPosts = () => (dispatch) => {
    dispatch({
        type: GET_POST_REQ
    });
    let limit = 25;
    fetch('https://jsonplaceholder.typicode.com/posts/')
        .then(response => response.json())
        .then(res => res.slice(0, limit))
        .then(responseData => {
            dispatch({
                type: GET_POST_RES,
                payload: responseData,
            })
        })
        .catch(err => {
            dispatch({
                type: GET_POST_ERR,
                payload: err,
            })
        })
}