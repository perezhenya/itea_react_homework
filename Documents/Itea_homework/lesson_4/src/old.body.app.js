<Route exact path="/account" component={Account} />
<Route path="/page" component={Pages} />
<Route path="/login" render={ ({ location }) => {
    console.log('loc', location)
    if( auth ){
        return( <Redirect to={location.state.returnPath} /> );
    }
    return(
        <div>
            <h1> Login form </h1>
            <button onClick={this.login}> Login in</button>
        </div>
    )
}} />
<Route exact path="/store" render={({ match, location, history }) => {
    console.log('match in render', auth, location );
    if( auth ){
        return(
            <h1> Store </h1>
        )
    } else {
        return(
            <Redirect to={{
                pathname: "/login",
                state: {
                    returnPath: location.pathname
                }
            }} />
        )
    }
}} />