import React from "react";

import { Switch, Route } from 'react-router-dom';
import NewsList from '../containers/NewsList';
import NewsItem from '../containers/NewsItem';

//конечные с exactom
//обертка


const NewsWrapper = () => (
    <Switch>
        <Route exact path="/posts/limit/30" component={NewsList} />
        <Route exact path="/posts/:id" component={NewsItem} />
    </Switch>
)


export default NewsWrapper;
