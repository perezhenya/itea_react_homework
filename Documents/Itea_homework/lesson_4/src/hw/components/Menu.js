import React from 'react'
import RootRouter from '../rootRouter';
import {BrowserRouter, Link, Switch, Route} from 'react-router-dom';
import '../../App.css';


const Menu = () => {

    return (
        <BrowserRouter>
            <div>
                <header>
                    <Link to='/'> MainPage</Link>
                    <Link to='/posts/limit/30'> Last News</Link>


                </header>
                <Switch>
                    {
                        RootRouter.map((route, key) => (
                            <Route key={key} {...route} />
                        ))

                    }

                </Switch>


            </div>
        </BrowserRouter>
    )
};


export default Menu;