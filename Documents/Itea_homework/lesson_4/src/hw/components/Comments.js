import React from 'react'

class  Comments extends React.Component {

    state = {
        listComments: [],
        // loaded: false,
    };



    getList = () => {
        let limit = 5;
        fetch(`https://jsonplaceholder.typicode.com/posts/1/comments`)
            .then(responseData => responseData.json())
            .then(data => data.slice(0, limit))
            .then(limitedData => {
                this.setState({
                    listComments: limitedData,
                });
            });
    }

    // componentDidUpdate(prevState) {
    //     if (this.state.listComments ===  prevState.listComments) {
    //    return false
    //     }
    // }
    // getList = () => {
    //     this.state.listComments.map(item => (
    //         console.log(item.name)
    //     ))
    // }

    render = () => {
        const {listComments, loaded} = this.state;
        const {getList} = this
        // if (!loaded) {
            return (
                <div className="col-7">
                    {/*<img src="https://media.giphy.com/media/3og0ICq6RIHdzjyDAI/giphy.gif"/> */}

                    <button onClick={getList} className="btn btn-primary">Show more comments</button>
                    <ul className="comment-item d-flex flex-column justify-content-center ">
                        {
                            listComments.map(item => (
                                <li className="comments-item" key={item.id}>
                                    {item.name}
                                    <h5>{item.body}</h5>
                                </li>

                            ))
                        }
                    </ul>

                </div>

)
        }


}


export default Comments;