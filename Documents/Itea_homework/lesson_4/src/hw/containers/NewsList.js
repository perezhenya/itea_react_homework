import React from 'react'
import { Link } from 'react-router-dom';
// import axios from 'axios';
import axios from '../../helpers/axios';

import ContextComponent from '../../components/ContextComponent';
//
// const Page = () => {
//     return (
//         <div>123</div>
//     )
// }




class NewsList extends React.Component {

    state = {
        news: [],
    }


    componentDidMount() {
        let limit = 20;
        axios.get('/posts/')
            .then(limitedData => {
                const news = limitedData.data.slice(0,limit);
                this.setState({news})
            }, error => {
                console.log(error)
            })

    }


    render = () => {
        const {news} = this.state;

        return (
            <div className="news-list">
                <h1 className="news-list-title"> NewsList </h1>
                <ol className="news-list-item">
                    {
                        news.map( item => (
                            <li key={item.id}>
                                <Link to={`/posts/${item.id}`}> {item.title} </Link>
                            </li>
                        ))
                    }
                </ol>

            </div>



        );


    }
};

// const MyHOC = ContextComponent ({title: '123'})(NewsList)


export default ContextComponent ({title: '123'})(NewsList);