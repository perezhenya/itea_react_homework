import React from 'react'
import Comments from '../components/Comments';


//matchparams

class  NewsItem extends React.Component {

    state = {
        listNews: [],
        loaded: false,
    }


    componentDidMount() {
        fetch(`https://jsonplaceholder.typicode.com/posts/${this.props.match.params.id}`)
            .then(responseData => responseData.json())
            .then(data => {
                this.setState({
                    listNews: data,
                });
            });
    }





    render = () => {
        const {listNews,loaded} = this.state;

        return (
            <div >
                <h1> Breaking news </h1>
                <ul className="list-item">
                    {
                        Object.keys(listNews).map((val, key) => (
                            <li key={key}>{listNews[val]}</li>
                        ))
                    }
                </ul>
                <Comments/>
               </div>
        )
    }
}

export default NewsItem;