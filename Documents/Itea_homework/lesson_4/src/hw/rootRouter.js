import MainPage from './containers/MainPage';
import NewsWrapper from './components/NewsWrapper';


export default [
    {
        path: '/',
        exact: true,
        component: MainPage,

    },

    {
        path: '/posts/',
        component: NewsWrapper,
    },



]