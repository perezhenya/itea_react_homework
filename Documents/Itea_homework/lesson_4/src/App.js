// import React from 'react';
//
// import { BrowserRouter, Route, Switch, Link, NavLink, Redirect, Prompt, withRouter } from 'react-router-dom';
//
// import RoutedPage, { Page1 }  from './Smsng';
//
// import RootRoutes from './rootRoutes';
// import './App.css';
//
// const supportsHistory = 'pushState' in window.history;
//
// const Pages = () => (
// 	<div>
// 		<h1> Pages </h1>
// 		<Route exact path="/page/:id" component={Page1} />
// 		<RoutedPage />
// 	</div>
// );
//
// class App extends React.Component {
//
// 	state = {
// 		auth: false
// 	}
//
// 	login = () => this.setState({ auth: true })
//
// 	render = () => {
// 		const { auth } = this.state;
// 		return(
// 			<BrowserRouter forceRefresh={!supportsHistory} >
// 				<div>
// 					<header>
// 						<NavLink to="/page/">Page 1</NavLink>
// 						<NavLink to="/page/150">Page 150</NavLink>
// 						<NavLink to="/account">Acount</NavLink>
//
// 						<NavLink to={{
// 							pathname: '/page/12',
// 							search: '?hello=true',
// 							state: {
// 								data: [1,2,3],
// 								string: "Hello"
// 							}
// 						}}
// 						activeStyle={{
// 							fontWeight: "bold",
// 							color: "red"
// 						}}> Link 0 </NavLink>
// 					</header>
// 					<Switch>
// 						<Redirect from="/account" to="/cabinet"/>
// 						{
// 							RootRoutes.map( route => (
// 								<Route
// 									{...route}
// 								/>
// 							))
// 						}
// 					</Switch>
// 				</div>
// 			</BrowserRouter>
// 		)
// 	}
//
// }
//
// export default App;

