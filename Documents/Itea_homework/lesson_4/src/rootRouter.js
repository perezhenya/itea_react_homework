import HomePage from './components/HomePage';
import ListWrapper from './components/ListWrapper';

export default [
    {
        path: '/',
        exact: true,
        component: HomePage,

    },

    {
        path: '/list',
        component: ListWrapper,
    },


]