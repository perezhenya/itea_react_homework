import React from 'react'
import { withRouter } from 'react-router-dom';

export const Page1 = ({ match, location }) => (
	<div>
		<h1> Page { match.params.id } </h1>
	</div>
)

export default withRouter(Page1);