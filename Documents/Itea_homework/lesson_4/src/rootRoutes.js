import React from 'react'


import MainPage from './components/Main';
import Account from './components/Account';
import NotFound from './components/NotFound';

export default [
    {
        path: '/',
        exact: true,
        component: MainPage
    },
    {
        path: '/cabinet',
        exact: true,
        component: Account
    },
    {
        path: '/rrr',
        exact: true,
        render: () => {
            return(
                <h1> RRR </h1>
            )
        }
    },
    {
        component: NotFound
    }

]