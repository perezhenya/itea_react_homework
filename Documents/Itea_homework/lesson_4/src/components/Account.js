import React from 'react'
import { Prompt } from 'react-router-dom';

const Account = () => (
	<>
		<h1> Account </h1>
		<Prompt message="Are you sure you want to leave?" />
	</>
)

export default Account;