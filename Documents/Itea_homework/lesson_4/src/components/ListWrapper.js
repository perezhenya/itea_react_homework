import React from "react";

import { Switch, Route } from 'react-router-dom';
import List from './List';
import ListItem from './ListItem';

//конечные с exactom
//обертка


const ListWrapper = () => (
    <Switch>
        <Route exact path="/list/" component={List} />
        <Route exact path="/list/:id" component={ListItem} />
    </Switch>
)


export default ListWrapper;
