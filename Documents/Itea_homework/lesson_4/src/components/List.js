import React from 'react'
import { Link } from 'react-router-dom';

class List extends React.Component {

   state = {
        users: [],
   }

    componentDidMount() {
        fetch('https://jsonplaceholder.typicode.com/users')
            .then(responseData => responseData.json())
            .then(data => {
                this.setState({
                    users: data,
                });
            });

}


    render = () => {
       const {users} = this.state;
        return (
            <div>
                <h1> List </h1>
                <ul>
                {
                    users.map( user => (
                        <li key={user.id}>
                            <Link to={`/list/${user.id}`}> {user.name} </Link>
                        </li>
                    ))
                }
                </ul>
            </div>


        );


    }
};


export default List;