import React from 'react'
import RootRouter from '../rootRouter';
import {BrowserRouter, Link, Switch, Route} from 'react-router-dom';


const Menu = () => {

return (
    <BrowserRouter>
        <div>
            <header>
                <Link to='/'> HomePage</Link>
                <Link to='/list'> List</Link>
                <Link to='/about'></Link>
                <Link to='/contacts'></Link>
            </header>
            <Switch>
                {
                    RootRouter.map((route, key) => (
                        <Route key={key} {...route} />
                    ))

                }

            </Switch>


        </div>
    </BrowserRouter>
)
};


export default Menu;