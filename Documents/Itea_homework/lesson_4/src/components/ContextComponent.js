import React from 'react'
import Helmet from 'react-helmet';



const ContextComponent = (params) => (Component) => {
    return () => (
        <>
        <Helmet>
            {params.title}
        </Helmet>
        <Component/>

        </>
    )

};


export default ContextComponent;