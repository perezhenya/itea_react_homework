import axios from 'axios';

const publicAxios = axios.create({
    baseURL : 'https://jsonplaceholder.typicode.com/',

});



export default publicAxios;