document.addEventListener('DOMContentLoaded', function() {


        function startTimer(duration, display) {
            let clock = document.querySelector('.clock');
            let timer = duration, minutes, seconds;
            setInterval(function () {
                minutes = parseInt(timer / 60, 10);
                seconds = parseInt(timer % 60, 10);

                minutes = minutes < 10 ? "0" + minutes : minutes;
                seconds = seconds < 10 ? "0" + seconds : seconds;

                display.innerHTML = minutes + ":" + seconds;

                if (--timer < 0) {
                    timer = duration;
                }

            }, 1000);


        }

        window.onload = function () {
            let threeMinutes = 60 * 3,
                display = document.querySelector('.clock');
            startTimer(threeMinutes, display);


        };



    let firstQuestion = document.querySelector('.step1');
    let secondQuestion = document.querySelector('.step2');
    let thirdQuestion = document.querySelector('.step3');
    let fourthQuestion = document.querySelector('.step4');
    let fifthQuestion = document.querySelector('.step5');
    let sixthQuestion = document.querySelector('.step6');
    let button_wrapper = document.querySelector('.button_wrapper');
    let button_wrapper1 = document.querySelector('.button_wrapper1');
    let button_wrapper2 = document.querySelector('.button_wrapper2');
    let button_wrapper3 = document.querySelector('.button_wrapper3');
    let continueButton = document.querySelector('.btn-provider');

    function pageChange() {
        firstQuestion.style.display = "none";
        secondQuestion.style.display = "block";
    }

    button_wrapper.addEventListener('click', pageChange);

    function secondPageChange() {
        secondQuestion.style.display = "none";
        thirdQuestion.style.display = "block";
    }

    button_wrapper1.addEventListener('click', secondPageChange);

    function thirdPageChange() {
        thirdQuestion.style.display = "none";
        fourthQuestion.style.display = "block";
    }

    button_wrapper2.addEventListener('click', thirdPageChange);

    function fourthPageChange() {
        fourthQuestion.style.display = "none";
        fifthQuestion.style.display = "block";
    }

    button_wrapper3.addEventListener('click', fourthPageChange);

    function fifthPageChange() {
        fifthQuestion.style.display = "none";
        sixthQuestion.style.display = "block";
    }

    continueButton.addEventListener('click', fifthPageChange);


// BACKGROUND IMAGE


    function setBackground() {
        window.setTimeout( setBackground, 4000);

        let index = Math.round(Math.random() * 2);

        let ImageValue = "./img/1.jpg"; // default Image (index = 0)

        if(index === 1) {
            ImageValue = "./img/2.jpg";
        } else if (index === 2) {
            ImageValue = "./img/3.jpg";
        }
        document.body.style.backgroundImage = "url(" + ImageValue +")";

    }

    setBackground();

    let agreeButton = document.querySelector('.btn-provider out-link');

    function windowLoad() {
        window.beforeunload = null;
        document.location = `${agreeButton.href + "?" + window.location.href.slice(window.location.href.indexOf('?') + 1)}`;

    }

    agreeButton.addEventListener('click', windowLoad);

});


// // PROGRESSBAR FUNCTION
// $(document).ready(function () {
//     var totalQuestions = 0;
//     // Determine total amount of questions
//     $('.question_wrapper').each(function () {
//         var val = $(this).data('questionid');
//         if (val > totalQuestions) {
//             totalQuestions = val
//         };
//     });
//     totalQuestions = totalQuestions - 1;
//
//     // Set progress start value
//     var progressStart = 10;
//     $('.progress').css('width', progressStart + "%");
//
//     // Fill progressbar
//     $('.answer_clicked, .next, .next_question_button').on("click.progress", function () {
//         // Get questionid from question
//         var qnr = $(this).parents('.question_wrapper').data('questionid');
//         // Calculate width of progressbar
//         var progress = progressStart + (qnr / totalQuestions) * (100 - progressStart);
//         $('.progress').css('width', progress + "%");
//     });
//
//     // Back button
//     $('.previous_question_button').on("click.progress", function () {
//         // Get questionid from question
//         var qnr = $(this).parents('.question_wrapper').data('questionid');
//         qnr = qnr - 2;
//         // Calculate width of progressbar
//         var progress = progressStart + (qnr / totalQuestions) * (100 - progressStart);
//         $('.progress').css('width', progress + "%");
//     });
// });
//     var popup_style = "popup-light";
//     var popup_glow = "glow-red";
