var translation = {
    source: {
        title: "DirtyTinder",
        text01: "Are you looking for hot dates in your neighbourhood?",
        text02: "Temporary offer",
        text03: "valid for:",
        text04: "Are you over 18 years old?",
        text05: "No",
        text06: "Yes",
        text07: "Most women here are single mothers and married wives looking for an affair. They could be your neighbour or someone you know. Can you keep the identities of these women a secret?",
        text08: "No",
        text09: "Yes",
        text10: "Our female members requested us to not admit male members looking for a 'relationship'. Many just want to fuck and enjoy casual hookups. Do you agree with this request?",
        text11: "No",
        text12: "Yes",
        text13: "Have you ever had an STD?",
        text14: "No",
        text15: "Yes",
        text16: "You can view the list of women in your neighbourhood to browse through their pictures. And again, please keep their identities a secret! Click on below button to continue.",
        text17: "Continue!"
    },
    en: {
        title: "DirtyTinder",
        text01: "Are you looking for hot dates in your neighbourhood?",
        text02: "Temporary offer",
        text03: "valid for:",
        text04: "Are you over 18 years old?",
        text05: "No",
        text06: "Yes",
        text07: "Most women here are single mothers and married wives looking for an affair. They could be your neighbour or someone you know. Can you keep the identities of these women a secret?",
        text08: "No",
        text09: "Yes",
        text10: "Our female members requested us to not admit male members looking for a 'relationship'. Many just want to fuck and enjoy casual hookups. Do you agree with this request?",
        text11: "No",
        text12: "Yes",
        text13: "Have you ever had an STD?",
        text14: "No",
        text15: "Yes",
        text16: "You can view the list of women in your neighbourhood to browse through their pictures. And again, please keep their identities a secret! Click on below button to continue.",
        text17: "Continue!"
    },
    de: {
        title: "DirtyTinder",
        text01: "Suchst du nach heiÃŸen Dates in deiner NÃ¤he?",
        text02: "Zeitlich befristetes Angebot",
        text03: "gÃ¼ltig fÃ¼r:",
        text04: "Bist du Ã¼ber 18 Jahre alt?",
        text05: "Nein",
        text06: "Ja",
        text07: "Viele dieser Frauen sind verzweifelte alleinerziehende MÃ¼tter und untreue Frauen auf der Suche nach VergnÃ¼gen. Sie kÃ¶nnten deine Nachbarn oder jemand sein, den du kennst. Stimmst du zu, die IdentitÃ¤t dieser Frauen geheim zu halten?",
        text08: "Nein",
        text09: "Ja",
        text10: "Diese Frauen haben uns gebeten, keine MÃ¤nner zu akzeptieren, die nach einer 'Beziehung' suchen. Sie wollen nur anonyme sexuelle Begegnungen. Keine Beziehung. Stimmen Sie dieser Anfrage zu?",
        text11: "Nein",
        text12: "Ja",
        text13: "Hattest du schon mal eine Geschlechtskrankheit?",
        text14: "Nein",
        text15: "Ja",
        text16: "Du kannst dir die Liste der Frauen in deiner Umgebung ansehen und ihre Bilder durchstÃ¶bern. Und noch einmal, bitte halte deren IdentitÃ¤ten geheim! Klicke auf den Button unten, um fortzufahren.",
        text17: "Fortsetzen!"
    },
    da: {
        title: "DirtyTinder",
        text01: "Leder du efter en hot date i dit omrÃ¥de?",
        text02: "Midlertidigt tilbud",
        text03: "gyldigt til:",
        text04: "Er du over 18 Ã¥r?",
        text05: "Nej",
        text06: "Ja",
        text07: "Mange af disse kvinder er desperate, enlige mÃ¸dre og utro hustruer der leder efter lidt sjov. De kan vÃ¦re dine naboer eller nogen du kender. ErklÃ¦rer du dig enig i at holde identiteten af disse kvinder hemmelig?",
        text08: "Nej",
        text09: "Ja",
        text10: "Disse kvinder har bedt os om at ekskludere mÃ¦nd der sÃ¸ger et â€™forholdâ€™. De Ã¸nsker kun anonyme seksuelle mÃ¸der. Ingen dating. Er du enig i denne anmodning?",
        text11: "Nej",
        text12: "Ja",
        text13: "Har du fÃ¸r haft en kÃ¸nssygdom?",
        text14: "Nej",
        text15: "Ja",
        text16: "Du kan se listen over kvinder i dit omrÃ¥de og kigge deres billeder igennem. Men husk at holde deres identitet hemmelig! Klik pÃ¥ knappen nedenfor for at forsÃ¦tte.",
        text17: "FortsÃ¦t!"
    },
    es: {
        title: "DirtyTinder",
        text01: "Â¿EstÃ¡s buscando alguna cita interesante por tu vecindario?",
        text02: "Oferta temporal",
        text03: "vÃ¡lido durante:",
        text04: "Â¿Tienes mÃ¡s de 18 aÃ±os?",
        text05: "No",
        text06: "SÃ¬",
        text07: "Muchas de estas mujeres son madres solteras desesperadas y esposas infieles en busca de diversiÃ³n. PodrÃ­an ser tus vecinos o alguien que conoces. Â¿EstÃ¡ de acuerdo en mantener en secreto la identidad de estas mujeres?",
        text08: "No",
        text09: "SÃ¬",
        text10: "Estas mujeres nos han pedido que no permitamos hombres que buscan una 'relaciÃ³n'. SÃ³lo desean encuentros sexuales anÃ³nimos. No estoy saliendo con nadie. Â¿EstÃ¡ de acuerdo con esta peticiÃ³n?",
        text11: "No",
        text12: "SÃ¬",
        text13: "Â¿Has tenido alguna vez alguna ETS?",
        text14: "No",
        text15: "SÃ¬",
        text16: "Puedes ver la lista de mujeres que hay en tu vecindario para hojear sus fotos. Â¡Recuerda mantener sus identidades en secreto! Haz click en el siguiente botÃ³n para continuar.",
        text17: "Continuar"
    },
    fi: {
        title: "DirtyTinder",
        text01: "EtsitkÃ¶ kuumia deittejÃ¤ alueellasi?",
        text02: "VÃ¤liaikainen tarjous",
        text03: "voimassa:",
        text04: "Oletko yli 18-vuotias?",
        text05: "Ei",
        text06: "KyllÃ¤",
        text07: "Monet naisista ovat epÃ¤toivoisia sinkkuÃ¤itejÃ¤ ja pettÃ¤viÃ¤ aviopuolisoja, jotka etsivÃ¤t hauskanpitoa. He saattavat olla naapureitasi tai muita henkilÃ¶itÃ¤, jotka tunnet. PidÃ¤thÃ¤n naisten identiteetin salaisuutena? ",
        text08: "Ei",
        text09: "KyllÃ¤",
        text10: "NÃ¤mÃ¤ naiset ovat pyytÃ¤neet meitÃ¤ poistamaan miehet, jotka etsivÃ¤t parisuhdetta. HeidÃ¤n ainoa halunsa on anonyymi seksuaalinen kanssakÃ¤ynti. Ei deittailua. Suostutko tÃ¤hÃ¤n pyyntÃ¶Ã¶n?",
        text11: "Ei",
        text12: "KyllÃ¤",
        text13: "Onko sinulla koskaan ollut sukupuolitautia?",
        text14: "Ei",
        text15: "KyllÃ¤",
        text16: "Voit katsoa luetteloa alueesi naisista ja selata heidÃ¤n kuviaan. Ja pidÃ¤thÃ¤n heidÃ¤n henkilÃ¶llisyytensÃ¤ salassa! Klikkaa alla olevaa painiketta jatkaaksesi.",
        text17: "Jatka!"
    },
    fr: {
        title: "DirtyTinder",
        text01: "Cherchez-vous des rendez-vous chauds dans votre rÃ©gion?",
        text02: "Offre temporaire",
        text03: "valide pendant:",
        text04: "Avez-vous plus de 18 ans?",
        text05: "Non",
        text06: "Oui",
        text07: "Plusieurs de ces femmes sont des mÃ¨res cÃ©libataires dÃ©sespÃ©rÃ©es et des femmes infidÃ¨les Ã  la recherche de plaisir. Elles pourraient Ãªtre vos voisines ou une personne que vous connaissez. Acceptez-vous de garder l'identitÃ© de ces femmes secrÃ¨te?",
        text08: "Non",
        text09: "Oui",
        text10: "Ces femmes nous ont fait la requÃªte de ne pas accepter des hommes Ã  la recherche dâ€™une Â«relationÂ». Elles ne dÃ©sirent que des rencontres sexuelles anonymes. Pas de frÃ©quentation. ÃŠtes-vous d'accord avec cette demande?",
        text11: "Non",
        text12: "Oui",
        text13: "Avez-vous dÃ©jÃ  eu une MST?",
        text14: "Non",
        text15: "Oui",
        text16: "Vous pouvez voir les femmes de votre rÃ©gion en naviguant Ã  travers leurs photos. Et n'oubliez pas, veuillez garder leur identitÃ©s secrÃ¨tes! Cliquez sur le bouton ci-dessous pour continuer.",
        text17: "Continuer!"
    },
    it: {
        title: "DirtyTinder",
        text01: "Sei alla ricerca di appuntamenti hot nel tuo quartiere?",
        text02: "Offerta temporanea",
        text03: "valida per:",
        text04: "Hai superato i 18 anni?",
        text05: "No",
        text06: "SÃ¬",
        text07: "Molte di queste donne sono mamme single disperate o mogli infedeli in cerca di divertimento. Potrebbero essere le tue vicine o qualcuno che conosci. Confermi di non rivelare lâ€™identitÃ  di queste donne?",
        text08: "No",
        text09: "SÃ¬",
        text10: "Queste donne ci hanno chiesto di non metterle in contatto con uomini che stanno cercando una â€œrelazioneâ€. Desiderano soltanto incontri sessuali anonimi. Nessuna frequentazione. Condividi questa richiesta?",
        text11: "No",
        text12: "SÃ¬",
        text13: "Hai mai avuto un STD?",
        text14: "No",
        text15: "SÃ¬",
        text16: "Puoi visualizzare un elenco di donne del tuo vicinato e sfogliare le loro immagini. Ti ricordiamo perÃ², di mantenere segrete le loro identitÃ ! Clicca sul pulsante qui sotto per continuare.",
        text17: "Continua!"
    },
    nl: {
        title: "DirtyTinder",
        text01: "Ben je op zoek naar een afspraak voor vanavond in jouw regio?",
        text02: "Aanbieding",
        text03: "geldig voor:",
        text04: "Heb je 18 jaar oud?",
        text05: "Nee",
        text06: "Ja",
        text07: "Veel van deze vrouwen zijn wanhopige alleenstaande moeders en ontrouwe echtgenotes die gewoon op zoek zijn naar plezier. Ze kunnen je buren zijn of iemand die je kent. Ben je het ermee eens om de identiteit van deze vrouwen geheim te houden?",
        text08: "Nee",
        text09: "Ja",
        text10: "Deze vrouwen hebben ons gevraagd om geen toegang te verlenen aan mannen die een 'relatie' zoeken. Ze willen alleen anonieme seksuele ontmoetingen. Geen relatie. Ben je het eens met deze voorwaarde?",
        text11: "Nee",
        text12: "Ja",
        text13: "Heb je ooit een SOA gehad?",
        text14: "Nee",
        text15: "Ja",
        text16: "Nu kunt u de lijst met vrouwen daar en uw gebied zien om hun foto's te zien. Vergeet niet je identiteit en geheimhouding te bewaren! Klikken starten en de knop Voortzetten.",
        text17: "Voortzetten!"
    },
    no: {
        title: "DirtyTinder",
        text01: "Leter du etter en heftig date i ditt nabolag?",
        text02: "Begrenset tilbud",
        text03: "gyldig til:",
        text04: "Er du over 18 Ã¥r?",
        text05: "Nei",
        text06: "Ja",
        text07: "Mange av kvinnene er desperate alenemÃ¸dre og utro koner som ser etter moro det kan vÃ¦re naboen eller noen du kjenner. Godtar du Ã¥ holde identiteten til disse kvinnene hemmelig?",
        text08: "Nei",
        text09: "Ja",
        text10: "Disse kvinnene har bedt oss om Ã¥ ikke tillate menn som er pÃ¥ jakt etter et 'forhold'. De vil bare ha heftig, anonyme seksuelle eventyr. Ingen dating. Godtar du denne forespÃ¸rselen?",
        text11: "Nei",
        text12: "Ja",
        text13: "Har du noen gang hatt en kjÃ¸nnssykdom?",
        text14: "Nei",
        text15: "Ja",
        text16: "Du kan se listen over kvinner i ditt omrÃ¥de ved Ã¥ bla gjennom bildene deres. Og husk, hold identiteten deres skjult! Klikk knappen under for Ã¥ fortsette",
        text17: "Fortsett!"
    },
    pt: {
        title: "DirtyTinder",
        text01: "Procurando encontros escaldantes na sua zona?",
        text02: "Oferta temporÃ¡ria",
        text03: "vÃ¡lida por:",
        text04: "Tem mais de 18 anos?",
        text05: "NÃ£o",
        text06: "Sim",
        text07: "Muitas destas mulheres sÃ£o mÃ£es solteiras desesperadas e mulheres casadas Ã  procura de alguma diversÃ£o. Podem ser suas vizinhas ou alguÃ©m que conhece. Concorda em manter em segredo a identidade destas mulheres?",
        text08: "NÃ£o",
        text09: "Sim",
        text10: "Estas mulheres pediram-nos para excluir homens que procuram um â€žrelacionamentoâ€œ. Elas desejam apenas marcar encontros sexuais anÃ³nimos. NÃ£o querem um compromisso. Concorda com este pedido?",
        text11: "NÃ£o",
        text12: "Sim",
        text13: "Alguma vez teve uma DST?",
        text14: "NÃ£o",
        text15: "Sim",
        text16: "Pode ver a lista de mulheres na sua zona para navegar pelas suas fotos. E, uma vez mais, mantenha em segredo as suas identidades! Clique no botÃ£o abaixo para continuar.",
        text17: "Continuar!"
    },
    ru: {
        title: "DirtyTinder",
        text01: "Ð˜Ñ‰ÐµÑ‚Ðµ Ð³Ð¾Ñ€ÑÑ‡Ð¸Ñ… ÑÐ²Ð¸Ð´Ð°Ð½Ð¸Ð¹ Ñ Ð¶ÐµÐ½Ñ‰Ð¸Ð½Ð°Ð¼Ð¸ Ð¿Ð¾ ÑÐ¾ÑÐµÐ´ÑÑ‚Ð²Ñƒ?",
        text02: "Ð‘ÐµÑÐ¿Ð»Ð°Ñ‚Ð½Ð°Ñ Ñ€ÐµÐ³Ð¸ÑÑ‚Ñ€Ð°Ñ†Ð¸Ñ!",
        text03: "Ð°ÐºÑ†Ð¸Ñ Ð·Ð°ÐºÐ¾Ð½Ñ‡Ð¸Ñ‚ÑÑ Ñ‡ÐµÑ€ÐµÐ·:",
        text04: "Ð’Ð°Ð¼ ÑƒÐ¶Ðµ Ð¸ÑÐ¿Ð¾Ð»Ð½Ð¸Ð»Ð¾ÑÑŒ 18 Ð»ÐµÑ‚?",
        text05: "ÐÐµÑ‚",
        text06: "Ð”Ð°",
        text07: "Ð‘Ð¾Ð»ÑŒÑˆÐ¸Ð½ÑÑ‚Ð²Ð¾ Ð¶ÐµÐ½Ñ‰Ð¸Ð½ Ð·Ð´ÐµÑÑŒ Ð¾Ð´Ð¸Ð½Ð¾ÐºÐ¸Ðµ Ð¼Ð°Ð¼Ð¾Ñ‡ÐºÐ¸ Ð¸Ð»Ð¸ Ð½ÐµÑƒÐ´Ð¾Ð²Ð»ÐµÑ‚Ð²Ð¾Ñ€ÐµÐ½Ð½Ñ‹Ðµ Ð¶ÐµÐ½Ñ‹, ÐºÐ¾Ñ‚Ð¾Ñ€Ñ‹Ðµ Ð¸Ñ‰ÑƒÑ‚ Ð¿Ñ€Ð¸ÐºÐ»ÑŽÑ‡ÐµÐ½Ð¸Ð¹. ÐžÐ½Ð¸ Ð¼Ð¾Ð³ÑƒÑ‚ Ð¾ÐºÐ°Ð·Ð°Ñ‚ÑŒÑÑ Ð²Ð°ÑˆÐ¸Ð¼Ð¸ ÑÐ¾ÑÐµÐ´ÑÐ¼Ð¸ Ð¸Ð»Ð¸ Ñ‚ÐµÐ¼Ð¸, ÐºÐ¾Ð³Ð¾ Ð²Ñ‹ Ð·Ð½Ð°ÐµÑ‚Ðµ. Ð’Ñ‹ ÑÐ¾Ð³Ð»Ð°ÑÐ½Ñ‹ ÑÐ¾Ñ…Ñ€Ð°Ð½ÑÑ‚ÑŒ ÐºÐ¾Ð½Ñ„Ð¸Ð´ÐµÐ½Ñ†Ð¸Ð°Ð»ÑŒÐ½Ð¾ÑÑ‚ÑŒ?",
        text08: "ÐÐµÑ‚",
        text09: "Ð”Ð°",
        text10: "ÐŸÐ¾ Ð¿Ñ€Ð¾ÑÑŒÐ±Ðµ Ð½Ð°ÑˆÐ¸Ñ… ÑƒÑ‡Ð°ÑÑ‚Ð½Ð¸Ñ† Ð¼Ñ‹ Ð½Ðµ Ð´Ð¾Ð¿ÑƒÑÐºÐ°ÐµÐ¼ Ð¼ÑƒÐ¶Ñ‡Ð¸Ð½, ÐºÐ¾Ñ‚Ð¾Ñ€Ñ‹Ðµ Ð¸Ñ‰ÑƒÑ‚ 'Ð¾Ñ‚Ð½Ð¾ÑˆÐµÐ½Ð¸Ð¹'. Ð‘Ð¾Ð»ÑŒÑˆÐ°Ñ Ñ‡Ð°ÑÑ‚ÑŒ Ð¶ÐµÐ½Ñ‰Ð¸Ð½ Ð¿Ñ€Ð¾ÑÑ‚Ð¾ Ñ…Ð¾Ñ‡ÐµÑ‚ Ñ‚Ñ€Ð°Ñ…Ð°Ñ‚ÑŒÑÑ Ð¸ Ð½Ð°ÑÐ»Ð°Ð¶Ð´Ð°Ñ‚ÑŒÑÑ ÑÐ»ÑƒÑ‡Ð°Ð¹Ð½Ñ‹Ð¼Ð¸ ÑÐ²ÑÐ·ÑÐ¼Ð¸. Ð’Ñ‹ ÑÐ¾Ð³Ð»Ð°ÑÐ½Ñ‹ Ñ Ñ‚Ð°ÐºÐ¸Ð¼ Ð¿Ð¾Ð»Ð¾Ð¶ÐµÐ½Ð¸ÐµÐ¼ Ð´ÐµÐ»?",
        text11: "ÐÐµÑ‚",
        text12: "Ð”Ð°",
        text13: "Ð£ Ð²Ð°Ñ Ð±Ñ‹Ð»Ð¸ Ð²ÐµÐ½ÐµÑ€Ð¸Ñ‡ÐµÑÐºÐ¸Ðµ Ð·Ð°Ð±Ð¾Ð»ÐµÐ²Ð°Ð½Ð¸Ñ?",
        text14: "ÐÐµÑ‚",
        text15: "Ð”Ð°",
        text16: "Ð¢ÐµÐ¿ÐµÑ€ÑŒ Ð²Ñ‹ Ð¼Ð¾Ð¶ÐµÑ‚Ðµ Ð¿Ð¾ÑÐ¼Ð¾Ñ‚Ñ€ÐµÑ‚ÑŒ ÑÐ¿Ð¸ÑÐ¾Ðº Ð¶ÐµÐ½Ñ‰Ð¸Ð½, Ð¶Ð¸Ð²ÑƒÑ‰Ð¸Ñ… Ð¿Ð¾ ÑÐ¾ÑÐµÐ´ÑÑ‚Ð²Ñƒ, Ð¸ ÑƒÐ²Ð¸Ð´ÐµÑ‚ÑŒ Ð¸Ñ… Ñ„Ð¾Ñ‚Ð¾Ð³Ñ€Ð°Ñ„Ð¸Ð¸. Ð˜ ÐµÑ‰Ðµ Ñ€Ð°Ð·: Ð¿Ð¾Ð¶Ð°Ð»ÑƒÐ¹ÑÑ‚Ð°, ÑÐ¾Ñ…Ñ€Ð°Ð½ÑÐ¹Ñ‚Ðµ Ð¸Ñ… ÐºÐ¾Ð½Ñ„Ð¸Ð´ÐµÐ½Ñ†Ð¸Ð°Ð»ÑŒÐ½Ð¾ÑÑ‚ÑŒ! ÐÐ°Ð¶Ð¼Ð¸Ñ‚Ðµ Ð½Ð° ÐºÐ½Ð¾Ð¿ÐºÑƒ Ð½Ð¸Ð¶Ðµ, Ñ‡Ñ‚Ð¾Ð±Ñ‹ Ð¿Ñ€Ð¾Ð´Ð¾Ð»Ð¶Ð¸Ñ‚ÑŒ.",
        text17: "ÐŸÑ€Ð¾Ð´Ð¾Ð»Ð¶Ð¸Ñ‚ÑŒ!"
    },
    sv: {
        title: "DirtyTinder",
        text01: "Letar du efter heta dejter nÃ¤ra dig?",
        text02: "TillfÃ¤lligt erbjudande",
        text03: "relevant fÃ¶r:",
        text04: "Ã„r du Ã¶ver 18 Ã¥r?",
        text05: "Nej",
        text06: "Ja",
        text07: "MÃ¥nga av dessa kvinnor Ã¤r desperata singel mammor eller otrogna fruar som letar efter lite Ã¤ventyr. De kan vara en av dina grannar eller nÃ¥gon du kÃ¤nner. Accepterar du att lÃ¥ta dessa kvinnors identitet fÃ¶rbli en hemlighet?",
        text08: "Nej",
        text09: "Ja",
        text10: "Dessa kvinnor har bett oss att inte tillÃ¥ta mÃ¤n som sÃ¶ker ett 'fÃ¶rhÃ¥llande'. De Ã¶nskar bara anonyma sexuella mÃ¶ten. Inte dejting. Accepterar du denna fÃ¶rfrÃ¥gan?",
        text11: "Nej",
        text12: "Ja",
        text13: "Har du nÃ¥gonsin haft en kÃ¶nssjukdom?",
        text14: "Nej",
        text15: "Ja",
        text16: "Du kan se listan Ã¶ver kvinnor nÃ¤ra dig fÃ¶r att kolla igenom deras bilder. Och igen, snÃ¤lla hÃ¥ll deras identiteter hemliga! Klicka pÃ¥ knappen nedan fÃ¶r att fortsÃ¤tta.",
        text17: "FortsÃ¤tt!"
    },
    tr: {
        title: "DirtyTinder",
        text01: "Ã‡evrendeki ateÅŸli hatunlarÄ± mÄ± arÄ±yorsun?",
        text02: "GeÃ§ici teklif",
        text03: "geÃ§erlilik sÃ¼resi:",
        text04: "18 yaÅŸÄ±ndan bÃ¼yÃ¼k mÃ¼sÃ¼n?",
        text05: "HayÄ±r",
        text06: "Evet",
        text07: "Bu kadÄ±nlarÄ±n Ã§oÄŸu azgÄ±n bekar hanÄ±mlar ve biraz eÄŸlence arayan, eÅŸleri aldatan ev kadÄ±nlarÄ±. KomÅŸularÄ±nÄ±z ya da tanÄ±dÄ±ÄŸÄ±nÄ±z biri olabilirler. Bu kadÄ±nlarÄ±n kimliÄŸini gizli tutmayÄ± kabul ediyor musunuz?",
        text08: "HayÄ±r",
        text09: "Evet",
        text10: "Bu kadÄ±nlar, 'iliÅŸki' arayÄ±ÅŸÄ±nda olan insanlarÄ± dÄ±ÅŸarÄ±da bÄ±rakmamÄ±zÄ± rica etti. Sadece anonim seks partnerleri istiyorlar, iliÅŸki deÄŸil. Bu talebi kabul ediyor musunuz?",
        text11: "HayÄ±r",
        text12: "Evet",
        text13: "Cinsel yollarla bulaÅŸan herhangi bir hastalÄ±k geÃ§irdin mi?",
        text14: "HayÄ±r",
        text15: "Evet",
        text16: "Resimlerine gÃ¶z atmak iÃ§in Ã§evrendeki bayanlarÄ±n listesini gÃ¶rÃ¼ntÃ¼leyebilirsin. LÃ¼tfen kimliklerini saklÄ± tut! Devam etmek iÃ§in aÅŸaÄŸÄ±daki dÃ¼ÄŸmeye tÄ±kla.",
        text17: "Sonraki!"
    },
    cs: {
        title: "DirtyTinder",
        text01: "HledÃ¡Å¡ Å¾havÃ© partnerky ve tvÃ©m okolÃ­?",
        text02: "DoÄasnÃ¡ nabÃ­dka,",
        text03: "platÃ­ pro:",
        text04: "Bylo ti uÅ¾ 18 let?",
        text05: "Ne",
        text06: "Ano",
        text07: "VÄ›tÅ¡ina Å¾en zde jsou svobodnÃ© matky a vdanÃ© Å¾eny, kterÃ© hledajÃ­ milostnÃ½ pomÄ›r. NÄ›kterÃ© z nich by mohly bÃ½t tvoje sousedky nebo nÄ›kdo, koho znÃ¡Å¡. DokÃ¡Å¾eÅ¡ udrÅ¾et totoÅ¾nost tÄ›chto Å¾en v tajnosti?",
        text08: "Ne",
        text09: "Ano",
        text10: "NaÅ¡e Älenky nÃ¡s poÅ¾Ã¡daly, abychom nepÅ™ijÃ­maly Äleny, kteÅ™Ã­ hledajÃ­ â€švztahâ€˜. MnohÃ© si jen chtÄ›jÃ­ zapÃ­chat a uÅ¾Ã­vat si, kdyÅ¾ nÄ›koho ulovÃ­. NevadÃ­ ti tento poÅ¾adavek?",
        text11: "Ne",
        text12: "Ano",
        text13: "MÄ›l jsi nÄ›kdy nÄ›jakou pohlavnÃ­ nemoc?",
        text14: "Ne",
        text15: "Ano",
        text16: "MÅ¯Å¾eÅ¡ se podÃ­vat na seznam Å¾en ve tvÃ©m okolÃ­ a prohlÃ­Å¾et si jejich fotky. A jeÅ¡tÄ› jednou, udrÅ¾uj jejich totoÅ¾nost v tajnosti, prosÃ­m! Klikni na tlaÄÃ­tko nÃ­Å¾e pro pokraÄovÃ¡nÃ­.",
        text17: "PokraÄovat!"
    }
};

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

function detect_language() {
    var forceLang = getParameterByName("lang");

    if (forceLang) {
        return forceLang;
    } else {
        var userLang = navigator.languages && navigator.languages[0] || navigator.language || navigator.userLanguage;
        if (userLang == "zh-CN" || userLang == "zh-SG" || userLang == "zh-MY" || userLang == "zh-CHS") {
            userLang = "zh-Hans";
        } else if (userLang == "zh-HK" || userLang == "zh-MO" || userLang == "zh-TW" || userLang == "zh-CHT") {
            userLang = "zh-Hant";
        } else if (userLang == "no" || userLang == "nb" || userLang == "nb-NO" || userLang == "nn-NO") {
            userLang = "no";
        } else if (userLang.length > 2) {
            userLang = userLang[0] + userLang[1];
        }
        return userLang;
    }
}

var language = 'source';
var browserLang = detect_language();

function replace_text(lang, text) {
    x = document.getElementById(text);
    if (x) {
        x.innerHTML = translation[lang][text];
    } else if ($('[data-placeholder-' + text + ']').length) {
        $('[data-placeholder-' + text + ']').attr('placeholder', translation[lang][text]);
    } /*else {
		console.log("element not Found: " + text);
	}*/
}

function translation_available(objTrls) {
    if (objTrls[browserLang]) {
        if(objTrls === "translation")
            language = browserLang; // for errors in main.js
        return browserLang;
    } else {
        console.log("translation not Found: " + browserLang);
        return "source";
    }
}

function translate() {
    var availableLang = translation_available(translation);

    for (var x in translation["source"]) {
        replace_text(availableLang, x);
    }
}

$(document).ready(function() {
    translate();
});
