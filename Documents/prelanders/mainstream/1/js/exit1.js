
/*!
 * JavaScript Cookie v2.1.0
 * https://github.com/js-cookie/js-cookie
 *
 * Copyright 2006, 2015 Klaus Hartl & Fagner Brack
 * Released under the MIT license
 */
!function(e){if("function"==typeof define&&define.amd)define(e);else if("object"==typeof exports)module.exports=e();else{var n=window.Cookies,t=window.Cookies=e();t.noConflict=function(){return window.Cookies=n,t}}}(function(){function e(){for(var e=0,n={};e<arguments.length;e++){var t=arguments[e];for(var o in t)n[o]=t[o]}return n}function n(t){function o(n,r,i){var c;if(arguments.length>1){if(i=e({path:"/"},o.defaults,i),"number"==typeof i.expires){var s=new Date;s.setMilliseconds(s.getMilliseconds()+864e5*i.expires),i.expires=s}try{c=JSON.stringify(r),/^[\{\[]/.test(c)&&(r=c)}catch(a){}return r=t.write?t.write(r,n):encodeURIComponent(String(r)).replace(/%(23|24|26|2B|3A|3C|3E|3D|2F|3F|40|5B|5D|5E|60|7B|7D|7C)/g,decodeURIComponent),n=encodeURIComponent(String(n)),n=n.replace(/%(23|24|26|2B|5E|60|7C)/g,decodeURIComponent),n=n.replace(/[\(\)]/g,escape),document.cookie=[n,"=",r,i.expires&&"; expires="+i.expires.toUTCString(),i.path&&"; path="+i.path,i.domain&&"; domain="+i.domain,i.secure?"; secure":""].join("")}n||(c={});for(var p=document.cookie?document.cookie.split("; "):[],d=/(%[0-9A-Z]{2})+/g,u=0;u<p.length;u++){var f=p[u].split("="),l=f[0].replace(d,decodeURIComponent),m=f.slice(1).join("=");'"'===m.charAt(0)&&(m=m.slice(1,-1));try{if(m=t.read?t.read(m,l):t(m,l)||m.replace(d,decodeURIComponent),this.json)try{m=JSON.parse(m)}catch(a){}if(n===l){c=m;break}n||(c[l]=m)}catch(a){}}return c}return o.get=o.set=o,o.getJSON=function(){return o.apply({json:!0},[].slice.call(arguments))},o.defaults={},o.remove=function(n,t){o(n,"",e(t,{expires:-1}))},o.withConverter=n,o}return n(function(){})});

/* docReady is a single plain javascript function that provides a method of scheduling one or more javascript functions to run at some later point when the DOM has finished loading. */
!function(t,e){"use strict";function n(){if(!a){a=!0;for(var t=0;t<o.length;t++)o[t].fn.call(window,o[t].ctx);o=[]}}function d(){"complete"===document.readyState&&n()}t=t||"docReady",e=e||window;var o=[],a=!1,c=!1;e[t]=function(t,e){return a?void setTimeout(function(){t(e)},1):(o.push({fn:t,ctx:e}),void("complete"===document.readyState||!document.attachEvent&&"interactive"===document.readyState?setTimeout(n,1):c||(document.addEventListener?(document.addEventListener("DOMContentLoaded",n,!1),window.addEventListener("load",n,!1)):(document.attachEvent("onreadystatechange",d),window.attachEvent("onload",n)),c=!0)))}}("docReady",window);

// exit modal window
if (typeof popup_style == 'undefined') {
    var popup_style = "popup-light";
}

if (typeof popup_glow == 'undefined') {
    var popup_glow = "glow-red";
}

var thePopup = '<div id="popup_exit" class="popup-modal '+ popup_style +'"><div class="modal-offer '+ popup_glow +'"><div class="modal-content"><div class="modal-text" id="popupText"></div></div></div></div><div class="popup_overlay"></div>';

var current_href = window.location.hostname;
var PreventExitSplash = false;

function getUrlParameter(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}
getUrlParameter("p") === "0" ? PreventExitSplash = true : PreventExitSplash = false;

// classes for text colors in <span class=""></span>: text-red, text-green, text-white, text-yellow
var alert_lang = {
    en: "<br/><br/>*********************************************<br/><br/>*** STOP! *** <br/><br/><span class='text-green'>GET EXTRA</span></strong> discounts and <strong><span class='text-green'>FREE</span></strong> shipping right now!</strong><br/><br/>Amazing new pills, best viagra alternative, make her <strong><span class='text-red'>SCREAM!</span></strong><br/><br/>Click <span class='text-green'>STAY ON PAGE</span></strong> below and get your benefits!<br/><br/>*********************************************<br/><br/>",
    fr: "<br/><br/>*********************************************<br/><br/>*** STOP! *** <br/><br/><span class='text-green'>OBTENEZ</span></strong> des remises <strong><span class='text-green'>SUPPLEMENTAIRES</span></strong> et la livraison des maintenant!</strong><br/><br/>De nouvelles pilules incroyables, la meilleure alternative au viagra, faites la <strong><span class='text-red'>CRIER!</span></strong><br/><br/>Cliquez sur <span class='text-green'>RESTER SUR LA PAGE</span></strong> ci-dessous et obtenez vos avantages!<br/><br/>*********************************************<br/><br/>",
    de: "<br/><br/>*********************************************<br/><br/>*** STOP! *** <br/><br/><span class='text-green'>ERHALTE JETZT EXTRA</span></strong> Rabatte und <strong><span class='text-green'>KOSTENLOSEN</span></strong> Versand!</strong><br/><br/>Unglaubliche neue Pillen, die beste Viagra-Alternative, bring sie zum <strong><span class='text-red'>Schreien!</span></strong><br/><br/>Klicke unten auf <span class='text-green'>AUF DER SEITE BLEIBEN</span></strong> und erhalte deine Vorteile!<br/><br/>*********************************************<br/><br/>",
    nl: "<br/><br/>*********************************************<br/><br/>*** STOP! *** <br/><br/><span class='text-green'>KRIJG NU EXTRA</span></strong> kortingen en <strong><span class='text-green'>GRATIS</span></strong> verzending!</strong><br/><br/>Verbazingwekkende nieuwe pillen, het beste viagra-alternatief, laat haar <strong><span class='text-red'>GILLEN!</span></strong><br/><br/>Klik hieronder op <span class='text-green'>BLIJF OP PAGINA</span></strong> en profiteer van je voordelen!<br/><br/>*********************************************<br/><br/>",
    it: "<br/><br/>*********************************************<br/><br/>*** ASPETTA! *** <br/><br/><span class='text-green'>RICEVI</span></strong> sconti <strong><span class='text-green'>EXTRA</span></strong> e spedizione gratuita adesso!</strong><br/><br/>Nuove fantastiche pillole, l'alternativa migliore al viagra, falla <strong><span class='text-red'>URLARE!</span></strong><br/><br/>Clicca <span class='text-green'>RESTA SULLA PAGINA</span></strong> e ricevi i tuoi benefici!<br/><br/>*********************************************<br/><br/>",
    es: "<br/><br/>*********************************************<br/><br/>*** Â¡DETENTE! *** <br/><br/>Â¡<span class='text-green'>OBTÃ‰N</span></strong> descuentos <strong><span class='text-green'>EXTRA</span></strong> y envÃ­o gratuito ahora mismo!</strong><br/><br/>Â¡Nuevos pÃ­ldoras increÃ­bles, la mejor viagra alternativa, hazla <strong><span class='text-red'>GRITAR DE PLACER!</span></strong><br/><br/>Â¡Haz clic en <span class='text-green'>PERMANECER EN LA PÃGINA</span></strong> en la parte inferior y obtÃ©n beneficios!<br/><br/>*********************************************<br/><br/>",
    no: "<br/><br/>*********************************************<br/><br/>*** STOP! *** <br/><br/><span class='text-green'>FÃ… EKSTRA</span></strong> avslag og <strong><span class='text-green'>GRATIS</span></strong> frakt nÃ¥!</strong><br/><br/>Fantastiske nye piller, det beste alternativet til Viagra, fÃ¥ henne til Ã¥ <strong><span class='text-red'>SKRIKE!</span></strong><br/><br/>Klikk <span class='text-green'>BLI PÃ… SIDEN</span></strong> under og motta dine fordeler!<br/><br/>*********************************************<br/><br/>",
    dk: "<br/><br/>*********************************************<br/><br/>*** STOP! *** <br/><br/><span class='text-green'>FÃ… EKSTRA</span></strong> rabatter og <strong><span class='text-green'>GRATIS</span></strong> levering lige nu!</strong><br/><br/>Fantastiske nye piller, bedste alternativ til viagra, fÃ¥ hende til at <strong><span class='text-red'>SKRIGE!</span></strong><br/><br/>Klik pÃ¥ <span class='text-green'>BLIV PÃ… SIDEN</span></strong> nedenfor og fÃ¥ dine fordele!<br/><br/>*********************************************<br/><br/>",
    da: "<br/><br/>*********************************************<br/><br/>*** STOP! *** <br/><br/><span class='text-green'>FÃ… EKSTRA</span></strong> rabatter og <strong><span class='text-green'>GRATIS</span></strong> levering lige nu!</strong><br/><br/>Fantastiske nye piller, bedste alternativ til viagra, fÃ¥ hende til at <strong><span class='text-red'>SKRIGE!</span></strong><br/><br/>Klik pÃ¥ <span class='text-green'>BLIV PÃ… SIDEN</span></strong> nedenfor og fÃ¥ dine fordele!<br/><br/>*********************************************<br/><br/>",
    fi: "<br/><br/>*********************************************<br/><br/>*** PYSÃ„HDY! *** <br/><br/><span class='text-green'>SAA LISÃ„Ã„</span></strong> alennuksia ja <strong><span class='text-green'>ILMAINEN</span></strong> toimitus juuri nyt!</strong><br/><br/>Mahtavia uusia pillereitÃ¤, parhaat viagran vaihtoehdot, saa hÃ¤net <strong><span class='text-red'>HUUTAMAAN!</span></strong><br/><br/>Klikkaa <span class='text-green'>PYSY SIVULLA</span></strong> alta ja saat etusi!<br/><br/>*********************************************<br/><br/>",
    tr: "<br/><br/>*********************************************<br/><br/>*** DUR! *** <br/><br/><span class='text-green'>ÅžÄ°MDÄ° ÃœCRETSÄ°Z</span></strong> kargo ve <strong><span class='text-green'>EXTRA</span></strong> indirim al!</strong><br/><br/>En iyi viagra alternatifi, inanÄ±lmaz yeni haplar, ona <strong><span class='text-red'>Ã‡IÄžLIK</span></strong> attÄ±r!<br/><br/>AÅŸaÄŸÄ±daki <span class='text-green'>SAYFADA KALâ€™a</span></strong> tÄ±kla ve avantajlardan faydalan!<br/><br/>*********************************************<br/><br/>",
    pt: "<br/><br/>*********************************************<br/><br/>*** PARE! *** <br/><br/><span class='text-green'>OBTENHA</span></strong> descontos <strong><span class='text-green'>EXTRA</span></strong> e portes gratuitos jÃ¡!</strong><br/><br/>Novas cÃ¡psulas surpreendentes, a melhor alternativa ao viagra, faÃ§a-a <strong><span class='text-red'>GEMER!</span></strong><br/><br/>Clique em <span class='text-green'>PERMANECER NA PÃGINA</span></strong> abaixo e consiga as suas vantagens!<br/><br/>*********************************************<br/><br/>",
    pl: "<br/><br/>*********************************************<br/><br/>*** CZEKAÄ†! *** NIE POZWALA uwolniÄ‡ KUPON EXPIRE!<br/><br/>Specjalna ZniÅ¼ka i *** *** wysyÅ‚ka GRATIS aktywowany.<br/><br/>Nowe oryginalne tabletki, lepiej niÅ¼ Viagra, Fuck na wiele godzin!<br/><br/>Kliknij poniÅ¼ej PAGE Stay On, aby otrzymaÄ‡ natychmiastowy rabat.<br/><br/>*********************************************<br/><br/>",
    cz: "<br/><br/>*********************************************<br/><br/>*** STOP! *** <br/><br/><span class='text-green'>ZÃSKEJTE EXTRA</span></strong> slevy a poÅ¡tovnÃ© <strong><span class='text-green'>ZDARMA</span></strong> prÃ¡vÄ› teÄ!</strong><br/><br/>NovÃ© ÃºÅ¾asnÃ© tablety, nejlepÅ¡Ã­ alternativa viagry, bude <strong><span class='text-red'>KÅ˜IÄŒET</span> blahem!</strong><br/><br/>KliknÄ›te nÃ­Å¾e na <span class='text-green'>ZÅ®STAT NA STRÃNCE</span></strong> a zÃ­skejte svÃ© vÃ½hody!<br/><br/>*********************************************<br/><br/>",
    cs: "<br/><br/>*********************************************<br/><br/>*** STOP! *** <br/><br/><span class='text-green'>ZÃSKEJTE EXTRA</span></strong> slevy a poÅ¡tovnÃ© <strong><span class='text-green'>ZDARMA</span></strong> prÃ¡vÄ› teÄ!</strong><br/><br/>NovÃ© ÃºÅ¾asnÃ© tablety, nejlepÅ¡Ã­ alternativa viagry, bude <strong><span class='text-red'>KÅ˜IÄŒET</span> blahem!</strong><br/><br/>KliknÄ›te nÃ­Å¾e na <span class='text-green'>ZÅ®STAT NA STRÃNCE</span></strong> a zÃ­skejte svÃ© vÃ½hody!<br/><br/>*********************************************<br/><br/>",
    hu: "<br/><br/>*********************************************<br/><br/>*** STOP! *** <br/><br/>Szerezzen <span class='text-green'>EGYEDI</span></strong> kedvezmenyeket Ã©s <strong><span class='text-green'>INGYENES SZÃLLÃTÃST</span></strong> most azonnal!</strong><br/><br/>ElkÃ©pesztÅ‘ Ãºj tabletta, a viagra legjobb alternatÃ­vÃ¡ja, <strong><span class='text-red'>SIKÃTSON</span></strong> Ã¶n miatt!<br/><br/>Kattintson az <span class='text-green'>OLDALON MARADOK</span></strong> gombra Ã©s szerezze meg kedvezmÃ©nyeit!<br/><br/>*********************************************<br/><br/>",
    sk: "<br/><br/>*********************************************<br/><br/>*** STOP! *** <br/><br/><span class='text-green'>ZÃSKAJTE EXTRA</span></strong> zÄ¾avy a doruÄenie <strong><span class='text-green'>ZADARMO</span></strong> prÃ¡ve teraz!</strong><br/><br/>SkvelÃ© novÃ© tabletky, najlepÅ¡ia alternatÃ­va Viagry, aby Å¾ena <strong><span class='text-red'>KRIÄŒALA!</span></strong><br/><br/>Kliknite na <span class='text-green'>OSTAÅ¤ NA STRÃNKE</span></strong>  a zÃ­skajte vaÅ¡e vÃ½hody!<br/><br/>*********************************************<br/><br/>",
    ro: "<br/><br/>*********************************************<br/><br/>*** STOP! *** <br/><br/><span class='text-green'>OBÅ¢INE EXTRA</span></strong> reduceri È™i beneficiazÄƒ de transport gratis chiar <strong><span class='text-green'>ACUM</span></strong>!</strong><br/><br/>Noile pastile uimitoare, cea mai bunÄƒ alternativÄƒ la Viagra, o vor face sÄƒ <strong><span class='text-red'>STRIGE</span></strong> de plÄƒcere!<br/><br/>ApasÄƒ butonul de mai jos <span class='text-green'>RÄ‚MÃ‚I PE PAGINÄ‚</span></strong> È™i beneficiazÄƒ de toate aceste avantaje!<br/><br/>*********************************************<br/><br/>",
    gr: "<br/><br/>*********************************************<br/><br/>*** Î£Î¤ÎŸÎ ! *** <br/><br/>Î›Î¬Î²ÎµÏ„Îµ <span class='text-green'>Î•ÎžÎ¤Î¡Î‘</span></strong> ÎµÎºÏ€Ï„ÏŽÏƒÎµÎ¹Ï‚ ÎºÎ±Î¹ <strong><span class='text-green'>Î”Î©Î¡Î•Î‘Î</span></strong> Î±Ï€Î¿ÏƒÏ„Î¿Î»Î® Ï„ÏŽÏÎ±!</strong><br/><br/>Î¤Î± ÎµÎºÏ€Î»Î·ÎºÏ„Î¹ÎºÎ¬ Î½Î­Î± Ï‡Î¬Ï€Î¹Î±, Î· ÎºÎ±Î»ÏÏ„ÎµÏÎ· ÎµÎ½Î±Î»Î»Î±ÎºÏ„Î¹ÎºÎ® Î»ÏÏƒÎ· Ï„Î¿Ï… viagra, ÎºÎ¬Î½Ï„Îµ Ï„Î·Î½ Î½Î± <strong><span class='text-red'>ÎŸÎ¥Î¡Î›Î™Î‘ÎžÎ•Î™!</span></strong><br/><br/>ÎšÎ¬Î½Ï„Îµ ÎºÎ»Î¹Îº ÏƒÏ„Î¿ <span class='text-green'>ÎœÎ•Î™ÎÎ•Î¤Î• Î£Î¤Î— Î£Î•Î›Î™Î”Î‘</span></strong> ÎºÎ±Î¹ Î»Î¬Î²ÎµÏ„Îµ Ï„Î± Î¿Ï†Î­Î»Î· ÏƒÎ±Ï‚!<br/><br/>*********************************************<br/><br/>",
    el: "<br/><br/>*********************************************<br/><br/>*** Î£Î¤ÎŸÎ ! *** <br/><br/>Î›Î¬Î²ÎµÏ„Îµ <span class='text-green'>Î•ÎžÎ¤Î¡Î‘</span></strong> ÎµÎºÏ€Ï„ÏŽÏƒÎµÎ¹Ï‚ ÎºÎ±Î¹ <strong><span class='text-green'>Î”Î©Î¡Î•Î‘Î</span></strong> Î±Ï€Î¿ÏƒÏ„Î¿Î»Î® Ï„ÏŽÏÎ±!</strong><br/><br/>Î¤Î± ÎµÎºÏ€Î»Î·ÎºÏ„Î¹ÎºÎ¬ Î½Î­Î± Ï‡Î¬Ï€Î¹Î±, Î· ÎºÎ±Î»ÏÏ„ÎµÏÎ· ÎµÎ½Î±Î»Î»Î±ÎºÏ„Î¹ÎºÎ® Î»ÏÏƒÎ· Ï„Î¿Ï… viagra, ÎºÎ¬Î½Ï„Îµ Ï„Î·Î½ Î½Î± <strong><span class='text-red'>ÎŸÎ¥Î¡Î›Î™Î‘ÎžÎ•Î™!</span></strong><br/><br/>ÎšÎ¬Î½Ï„Îµ ÎºÎ»Î¹Îº ÏƒÏ„Î¿ <span class='text-green'>ÎœÎ•Î™ÎÎ•Î¤Î• Î£Î¤Î— Î£Î•Î›Î™Î”Î‘</span></strong> ÎºÎ±Î¹ Î»Î¬Î²ÎµÏ„Îµ Ï„Î± Î¿Ï†Î­Î»Î· ÏƒÎ±Ï‚!<br/><br/>*********************************************<br/><br/>",
    ru: "<br/><br/>*********************************************<br/><br/>*** Ð¡Ð¢ÐžÐ™! *** <br/><br/>Ð—Ð°Ð±Ð¸Ñ€Ð°Ð¹ Ð´Ð¾Ð¿Ð¾Ð»Ð½Ð¸Ñ‚ÐµÐ»ÑŒÐ½ÑƒÑŽ <span class='text-green'>Ð¡ÐšÐ˜Ð”ÐšÐ£</span></strong> Ð¸ <strong><span class='text-green'>Ð‘Ð•Ð¡ÐŸÐ›ÐÐ¢ÐÐ£Ð® Ð”ÐžÐ¡Ð¢ÐÐ’ÐšÐ£</span></strong> Ð¿Ñ€ÑÐ¼Ð¾ ÑÐµÐ¹Ñ‡Ð°Ñ!</strong><br/><br/>ÐÐµÐ²ÐµÑ€Ð¾ÑÑ‚Ð½Ñ‹Ðµ, Ð½Ð¾Ð²Ñ‹Ðµ Ñ‚Ð°Ð±Ð»ÐµÑ‚ÐºÐ¸, Ð»ÑƒÑ‡ÑˆÐ°Ñ Ð°Ð»ÑŒÑ‚ÐµÑ€Ð½Ð°Ñ‚Ð¸Ð²Ð° Ð²Ð¸Ð°Ð³Ñ€Ðµ, Ð·Ð°ÑÑ‚Ð°Ð²ÑÑ‚ ÐµÐµ <strong><span class='text-red'>ÐšÐ Ð˜Ð§ÐÐ¢Ð¬!</span></strong><br/><br/>ÐÐ°Ð¶Ð¼Ð¸ <span class='text-green'>ÐžÐ¡Ð¢ÐÐ¢Ð¬Ð¡Ð¯ ÐÐ Ð¡Ð¢Ð ÐÐÐ˜Ð¦Ð•</span></strong>, Ñ‡Ñ‚Ð¾Ð±Ñ‹ Ð¿Ð¾Ð»ÑƒÑ‡Ð¸Ñ‚ÑŒ ÑÐºÐ¸Ð´ÐºÑƒ!<br/><br/>*********************************************<br/><br/>",
    id: "<br/><br/>*********************************************<br/><br/>*** BERHENTI! *** <br/><br/><span class='text-green'>DAPATKAN LEBIH</span></strong> diskaun dan penghantaran <strong><span class='text-green'>PERCUMA SEKARANG!</span></strong></strong><br/><br/>Pill baru yang menakjubkan, alternatif Viagra terbaik, buat si dia <strong><span class='text-red'>MENJERIT!</span></strong><br/><br/>Klik <span class='text-green'>TINGGAL PADA HALAMAN</span></strong> di bawah dan dapatkan faedah-faedah anda!<br/><br/>*********************************************<br/><br/>",
    th: "<br/><br/>*********************************************<br/><br/>*** à¸«à¸¢à¸¸à¸”à¸à¹ˆà¸­à¸™! *** <br/><br/>à¸£à¸±à¸šà¸ªà¹ˆà¸§à¸™à¸¥à¸”à¹€à¸žà¸´à¹ˆà¸¡à¹€à¸•à¸´à¸¡à¹à¸¥à¸°à¸ˆà¸±à¸”à¸ªà¹ˆà¸‡à¸Ÿà¸£à¸µà¸•à¸­à¸™à¸™à¸µà¹‰à¹€à¸¥à¸¢!<br/><br/>à¸¢à¸²à¹ƒà¸«à¸¡à¹ˆà¸—à¸µà¹ˆà¸™à¹ˆà¸²à¸•à¸·à¹ˆà¸™à¹€à¸•à¹‰à¸™ à¸—à¸²à¸‡à¹€à¸¥à¸·à¸­à¸à¹„à¸§à¸­à¸²à¸à¸£à¹‰à¸²à¸—à¸µà¹ˆà¸”à¸µà¸—à¸µà¹ˆà¸ªà¸¸à¸” à¸—à¸³à¹ƒà¸«à¹‰à¹€à¸˜à¸­à¸•à¹‰à¸­à¸‡à¸£à¹‰à¸­à¸‡à¸‚à¸­à¸Šà¸µà¸§à¸´à¸•!<br/><br/>à¸„à¸¥à¸´à¸ à¸­à¸¢à¸¹à¹ˆà¹ƒà¸™à¸«à¸™à¹‰à¸²à¸™à¸µà¹‰ à¸”à¹‰à¸²à¸™à¸¥à¹ˆà¸²à¸‡à¹à¸¥à¸°à¸£à¸±à¸šà¸ªà¸´à¸—à¸˜à¸´à¸›à¸£à¸°à¹‚à¸¢à¸Šà¸™à¹Œà¸‚à¸­à¸‡à¸„à¸¸à¸“!<br/><br/>*********************************************<br/><br/>",
    vn: "<br/><br/>*********************************************<br/><br/>*** CHá»œ Äá»¢I! *** KHÃ”NG CHO MIá»„N PHÃ PHIáº¾U GIáº¢M GIÃ Cá»¦A Báº N EXPIRE!<br/><br/>Giáº£m giÃ¡ Ä‘áº·c biá»‡t vÃ  *** MIá»„N PHÃ *** Váº­n Chuyá»ƒn hoáº¡t.<br/><br/>Thuá»‘c gá»‘c má»›i, tá»‘t hÆ¡n Viagra, fuck VÃ­ giá»!<br/><br/>Nháº¥n TRANG TRÃš ON dÆ°á»›i Ä‘Ã¢y Ä‘á»ƒ nháº­n Ä‘Æ°á»£c giáº£m giÃ¡ ngay láº­p tá»©c cá»§a báº¡n.<br/><br/>*********************************************<br/><br/>",
    vi: "<br/><br/>*********************************************<br/><br/>*** CHá»œ Äá»¢I! *** KHÃ”NG CHO MIá»„N PHÃ PHIáº¾U GIáº¢M GIÃ Cá»¦A Báº N EXPIRE!<br/><br/>Giáº£m giÃ¡ Ä‘áº·c biá»‡t vÃ  *** MIá»„N PHÃ *** Váº­n Chuyá»ƒn hoáº¡t.<br/><br/>Thuá»‘c gá»‘c má»›i, tá»‘t hÆ¡n Viagra, fuck VÃ­ giá»!<br/><br/>Nháº¥n TRANG TRÃš ON dÆ°á»›i Ä‘Ã¢y Ä‘á»ƒ nháº­n Ä‘Æ°á»£c giáº£m giÃ¡ ngay láº­p tá»©c cá»§a báº¡n.<br/><br/>*********************************************<br/><br/>",
    bg: "<br/><br/>*********************************************<br/><br/>*** Ð¡ÐŸÐ Ð•Ð¢Ð•! *** <br/><br/>ÐŸÐ¾Ð»ÑƒÑ‡ÐµÑ‚Ðµ <span class='text-green'>Ð”ÐžÐŸÐªÐ›ÐÐ˜Ð¢Ð•Ð›ÐÐ˜</span></strong> Ð¾Ñ‚ÑÑ‚ÑŠÐ¿ÐºÐ¸ Ð¸ <strong><span class='text-green'>Ð‘Ð•Ð—ÐŸÐ›ÐÐ¢ÐÐ</span></strong> Ð´Ð¾ÑÑ‚Ð°Ð²ÐºÐ° ÑÐµÐ³Ð°!</strong><br/><br/>ÐÐµÐ²ÐµÑ€Ð¾ÑÑ‚Ð½Ð¸ Ð½Ð¾Ð²Ð¸ Ñ…Ð°Ð¿Ñ‡ÐµÑ‚Ð°, Ð½Ð°Ð¹-Ð´Ð¾Ð±Ñ€Ð°Ñ‚Ð° Ð°Ð»Ñ‚ÐµÑ€Ð½Ð°Ñ‚Ð¸Ð²Ð° Ð½Ð° Ð²Ð¸Ð°Ð³Ñ€Ð°, Ð½Ð°ÐºÐ°Ñ€Ð°Ð¹Ñ‚Ðµ Ñ Ð´Ð° <strong><span class='text-red'>ÐšÐ Ð•Ð©Ð˜!</span></strong><br/><br/>ÐÐ°Ñ‚Ð¸ÑÐ½ÐµÑ‚Ðµ Ð²ÑŠÑ€Ñ…Ñƒ <span class='text-green'>ÐžÐ¡Ð¢ÐÐÐ˜ ÐÐ Ð¡Ð¢Ð ÐÐÐ˜Ð¦ÐÐ¢Ð</span></strong> Ð´Ð¾Ð»Ñƒ Ð¸ Ð¿Ð¾Ð»ÑƒÑ‡ÐµÑ‚Ðµ ÑÐ²Ð¾Ð¸Ñ‚Ðµ Ð¿ÐµÑ‡Ð°Ð»Ð±Ð¸!<br/><br/>*********************************************<br/><br/>",
    rs: "<br/><br/>*********************************************<br/><br/>*** STOP! *** <br/><br/><span class='text-green'>OSVOJITE DODATNE</span></strong> popuste i <strong><span class='text-green'>BESPLATNO</span></strong> slanje sada!</strong><br/><br/>Neverovatne nove pilule, najbolja zamena za vijagru, naterajte je da <strong><span class='text-red'>VRIÅ TI!</span></strong><br/><br/>Kliknite  <span class='text-green'>OSTANI NA STRANICI</span></strong> ispod i osvojite vase pogodnisti!<br/><br/>*********************************************<br/><br/>",
    sr: "<br/><br/>*********************************************<br/><br/>*** STOP! *** <br/><br/><span class='text-green'>OSVOJITE DODATNE</span></strong> popuste i <strong><span class='text-green'>BESPLATNO</span></strong> slanje sada!</strong><br/><br/>Neverovatne nove pilule, najbolja zamena za vijagru, naterajte je da <strong><span class='text-red'>VRIÅ TI!</span></strong><br/><br/>Kliknite  <span class='text-green'>OSTANI NA STRANICI</span></strong> ispod i osvojite vase pogodnisti!<br/><br/>*********************************************<br/><br/>",
    ph: "<br/><br/>*********************************************<br/><br/>*** WAIT! *** HUWAG HAYAAN ANG FREE Na!<br/><br/>COUPON mawawalan ng bisa!<br/><br/>Espesyal na Diskwento at *** LIBRENG *** Pagpapadala activate.<br/><br/>New Original Pills, Better Than Viagra, Fuck For Hours! I-click STAY ON PAGE ibaba upang makatanggap ng iyong mga instant discount.<br/><br/>*********************************************<br/><br/>",
    fl: "<br/><br/>*********************************************<br/><br/>*** WAIT! *** HUWAG HAYAAN ANG FREE Na!<br/><br/>COUPON mawawalan ng bisa!<br/><br/>Espesyal na Diskwento at *** LIBRENG *** Pagpapadala activate.<br/><br/>New Original Pills, Better Than Viagra, Fuck For Hours! I-click STAY ON PAGE ibaba upang makatanggap ng iyong mga instant discount.<br/><br/>*********************************************<br/><br/>",
    hr: "<br/><br/>*********************************************<br/><br/>*** STOP! *** <br/><br/><span class='text-green'>OSVOJITE DODATNE</span></strong> popuste i <strong><span class='text-green'>BESPLATNO</span></strong> slanje sada!</strong><br/><br/>Neverovatne nove pilule, najbolja zamena za vijagru, naterajte je da <strong><span class='text-red'>VRIÅ TI!</span></strong><br/><br/>Kliknite  <span class='text-green'>OSTANI NA STRANICI</span></strong> ispod i osvojite vase pogodnisti!<br/><br/>*********************************************<br/><br/>",
    se: "<br/><br/>*********************************************<br/><br/>*** STOPP! *** <br/><br/>FÃ¥ <span class='text-green'>EXTRA</span></strong> rabatter och <strong><span class='text-green'>GRATIS</span></strong> frakt hÃ¤r och nu!</strong><br/><br/>Fantastiska nya piller, bÃ¤sta alternativet till Viagra, fÃ¥ henne att <strong><span class='text-red'>VRÃ…LA!</span></strong><br/><br/>Klicka pÃ¥ <span class='text-green'>STANNA PÃ… SIDAN</span></strong> nedanfÃ¶r fÃ¶r att ta del av dina fÃ¶rmÃ¥ner!<br/><br/>*********************************************<br/><br/>",
    sv: "<br/><br/>*********************************************<br/><br/>*** STOPP! *** <br/><br/>FÃ¥ <span class='text-green'>EXTRA</span></strong> rabatter och <strong><span class='text-green'>GRATIS</span></strong> frakt hÃ¤r och nu!</strong><br/><br/>Fantastiska nya piller, bÃ¤sta alternativet till Viagra, fÃ¥ henne att <strong><span class='text-red'>VRÃ…LA!</span></strong><br/><br/>Klicka pÃ¥ <span class='text-green'>STANNA PÃ… SIDAN</span></strong> nedanfÃ¶r fÃ¶r att ta del av dina fÃ¶rmÃ¥ner!<br/><br/>*********************************************<br/><br/>",
    ms: "<br/><br/>*********************************************<br/><br/>*** BERHENTI! *** <br/><br/><span class='text-green'>DAPATKAN LEBIH</span></strong> diskaun dan penghantaran <strong><span class='text-green'>PERCUMA SEKARANG!</span></strong></strong><br/><br/>Pill baru yang menakjubkan, alternatif Viagra terbaik, buat si dia <strong><span class='text-red'>MENJERIT!</span></strong><br/><br/>Klik <span class='text-green'>TINGGAL PADA HALAMAN</span></strong> di bawah dan dapatkan faedah-faedah anda!<br/><br/>*********************************************<br/><br/>",
    my: "<br/><br/>*********************************************<br/><br/>*** BERHENTI! *** <br/><br/><span class='text-green'>DAPATKAN LEBIH</span></strong> diskaun dan penghantaran <strong><span class='text-green'>PERCUMA SEKARANG!</span></strong></strong><br/><br/>Pill baru yang menakjubkan, alternatif Viagra terbaik, buat si dia <strong><span class='text-red'>MENJERIT!</span></strong><br/><br/>Klik <span class='text-green'>TINGGAL PADA HALAMAN</span></strong> di bawah dan dapatkan faedah-faedah anda!<br/><br/>*********************************************<br/><br/>",
    ka: "<br/><br/>*********************************************<br/><br/>*** áƒ¨áƒ”áƒ©áƒ”áƒ áƒ“áƒ˜áƒ—! *** <br/><br/>áƒ›áƒ˜áƒ˜áƒ¦áƒ”áƒ— áƒ“áƒáƒ›áƒáƒ¢áƒ”áƒ‘áƒ˜áƒ—áƒ˜ áƒ¤áƒáƒ¡áƒ“áƒáƒ™áƒšáƒ”áƒ‘áƒ áƒ“áƒ áƒ£áƒ¤áƒáƒ¡áƒ áƒ›áƒ˜áƒ¢áƒáƒœáƒ áƒáƒ®áƒšáƒáƒ•áƒ”!<br/><br/>áƒ¡áƒáƒáƒªáƒáƒ áƒ˜ áƒáƒ®áƒáƒšáƒ˜ áƒ¢áƒáƒ‘áƒšáƒ”áƒ¢áƒ”áƒ‘áƒ˜, áƒ•áƒ˜áƒáƒ’áƒ áƒáƒ¡ áƒ¡áƒáƒ£áƒ™áƒ”áƒ—áƒ”áƒ¡áƒ áƒáƒšáƒ¢áƒ”áƒ áƒáƒœáƒ¢áƒ˜áƒ•áƒ, áƒáƒ™áƒ˜áƒ•áƒšáƒ”áƒ— áƒ˜áƒ¡!<br/><br/>áƒ“áƒáƒáƒ­áƒ˜áƒ áƒ”áƒ— áƒ’áƒ•áƒ”áƒ áƒ“áƒ–áƒ” áƒ“áƒáƒ áƒ©áƒ”áƒœáƒáƒ¡ áƒ¥áƒ•áƒ”áƒ›áƒáƒ— áƒ“áƒ áƒ›áƒ˜áƒ˜áƒ¦áƒ”áƒ— áƒ¡áƒáƒ áƒ’áƒ”áƒ‘áƒ”áƒšáƒ˜!<br/><br/>*********************************************<br/><br/>",
    gr: "<br/><br/>*********************************************<br/><br/>*** áƒ¨áƒ”áƒ©áƒ”áƒ áƒ“áƒ˜áƒ—! *** <br/><br/>áƒ›áƒ˜áƒ˜áƒ¦áƒ”áƒ— áƒ“áƒáƒ›áƒáƒ¢áƒ”áƒ‘áƒ˜áƒ—áƒ˜ áƒ¤áƒáƒ¡áƒ“áƒáƒ™áƒšáƒ”áƒ‘áƒ áƒ“áƒ áƒ£áƒ¤áƒáƒ¡áƒ áƒ›áƒ˜áƒ¢áƒáƒœáƒ áƒáƒ®áƒšáƒáƒ•áƒ”!<br/><br/>áƒ¡áƒáƒáƒªáƒáƒ áƒ˜ áƒáƒ®áƒáƒšáƒ˜ áƒ¢áƒáƒ‘áƒšáƒ”áƒ¢áƒ”áƒ‘áƒ˜, áƒ•áƒ˜áƒáƒ’áƒ áƒáƒ¡ áƒ¡áƒáƒ£áƒ™áƒ”áƒ—áƒ”áƒ¡áƒ áƒáƒšáƒ¢áƒ”áƒ áƒáƒœáƒ¢áƒ˜áƒ•áƒ, áƒáƒ™áƒ˜áƒ•áƒšáƒ”áƒ— áƒ˜áƒ¡!<br/><br/>áƒ“áƒáƒáƒ­áƒ˜áƒ áƒ”áƒ— áƒ’áƒ•áƒ”áƒ áƒ“áƒ–áƒ” áƒ“áƒáƒ áƒ©áƒ”áƒœáƒáƒ¡ áƒ¥áƒ•áƒ”áƒ›áƒáƒ— áƒ“áƒ áƒ›áƒ˜áƒ˜áƒ¦áƒ”áƒ— áƒ¡áƒáƒ áƒ’áƒ”áƒ‘áƒ”áƒšáƒ˜!<br/><br/>*********************************************<br/><br/>",
    lt: "<br/><br/>*********************************************<br/><br/>*** SUSTOKITE! *** <br/><br/><span class='text-green'>GAUKITE PAPILDOMÅ²</span></strong> nuolaidÅ³ ir <strong><span class='text-green'>NEMOKAMÄ„</span></strong> siuntimÄ… jau dabar!</strong><br/><br/>Nepakartojamos naujosios tabletÄ—s, geriausia alternatyva Viagrai, priverskite jÄ… <strong><span class='text-red'>Å AUKTI!</span></strong><br/><br/>Paspauskite Å¾emiau esantÄ¯ mygtukÄ… <span class='text-green'>LIKTI PUSLAPYJE</span></strong> ir atraskite savo naudÄ…!<br/><br/>*********************************************<br/><br/>",
    lv: "<br/><br/>*********************************************<br/><br/>*** STÄ€T! *** <br/><br/><span class='text-green'>SAÅ…EMT PAPILDUS</span></strong> atlaides un <strong><span class='text-green'>BEZMAKSAS</span></strong> piegÄdi jau tagad!</strong><br/><br/>Jaunas pÄrsteidzoÅ¡as tabletes, vislabÄkÄ alternatÄ«va tabletÄ“m viagra, lÄ«dz viÅ†a <strong><span class='text-red'>KLIEDZ!</span></strong><br/><br/>Nospiediet <span class='text-green'>PALIKT INTERNETA LAPÄ€</span></strong> zemÄk un saÅ†emiet savas priekÅ¡rocÄ«bas!<br/><br/>*********************************************<br/><br/>",
    jp: "<br/><br/>*********************************************<br/><br/>*** STOP! *** <br/><br/>ã•ã‚ã€ç‰¹åˆ¥å‰²å¼•ã¨é€æ–™ç„¡æ–™ã®ç‰¹å…¸ã‚’ç²å¾—ã—ã¾ã—ã‚‡ã†ï¼<br/><br/>æ–°ã—ãç´ æ™´ã‚‰ã—ã„ãƒ”ãƒ«ã€å„ªã‚ŒãŸãƒã‚¤ã‚¢ã‚°ãƒ©ä»£æ›¿è–¬ã§å½¼å¥³ã‚’å¤§ã„ã«å–œã°ã›ã¦ãã ã•ã„ï¼<br/><br/>ä¸‹ã®â€ãƒšãƒ¼ã‚¸ã«ç•™ã¾ã‚‹â€ã‚’ã‚¯ãƒªãƒƒã‚¯ã—ã¦ç‰¹å…¸ã‚’ã‚²ãƒƒãƒˆã—ã¾ã—ã‚‡ã†ï¼<br/><br/>*********************************************<br/><br/>",
    ja: "<br/><br/>*********************************************<br/><br/>*** STOP! *** <br/><br/>ã•ã‚ã€ç‰¹åˆ¥å‰²å¼•ã¨é€æ–™ç„¡æ–™ã®ç‰¹å…¸ã‚’ç²å¾—ã—ã¾ã—ã‚‡ã†ï¼<br/><br/>æ–°ã—ãç´ æ™´ã‚‰ã—ã„ãƒ”ãƒ«ã€å„ªã‚ŒãŸãƒã‚¤ã‚¢ã‚°ãƒ©ä»£æ›¿è–¬ã§å½¼å¥³ã‚’å¤§ã„ã«å–œã°ã›ã¦ãã ã•ã„ï¼<br/><br/>ä¸‹ã®â€ãƒšãƒ¼ã‚¸ã«ç•™ã¾ã‚‹â€ã‚’ã‚¯ãƒªãƒƒã‚¯ã—ã¦ç‰¹å…¸ã‚’ã‚²ãƒƒãƒˆã—ã¾ã—ã‚‡ã†ï¼<br/><br/>*********************************************<br/><br/>",
    et: "<br/><br/>*********************************************<br/><br/>*** STOPP! *** <br/><br/><span class='text-green'>SAA LISA</span></strong> allahindlust ja <strong><span class='text-green'>TASUTA</span></strong> saatmine kohe!</strong><br/><br/>Imelised uued tabletid, parim viagra alternatiiv, pane ta <strong><span class='text-red'>KARJUMA!</span></strong><br/><br/>Vajuta <span class='text-green'>JÃ„Ã„ LEHELE</span></strong> ja saa ome preemia!<br/><br/>*********************************************<br/><br/>",
    ar: "<br/><br/>*********************************************<br/><br/>*** ØªÙˆÙ‚Ù! *** <br/><br/>Ø§Ø­ØµÙ„ Ø¹Ù„Ù‰ Ø§Ù„Ù…Ø²ÙŠØ¯ Ù…Ù† Ø§Ù„Ø®ØµÙˆÙ…Ø§Øª ÙˆØ§Ù„Ø´Ø­Ù† Ø§Ù„Ù…Ø¬Ø§Ù†ÙŠ Ø§Ù„Ø¢Ù†!<br/><br/>Ø­Ø¨ÙˆØ¨ Ø¬Ø¯ÙŠØ¯Ø©ØŒ Ø§ÙØ¶Ù„ Ø¨Ø¯ÙŠÙ„ Ù„Ù„ÙÙŠØ§Ø¬Ø±Ø§ØŒ Ù…ØªØ¹Ù‡Ø§ Ø£ÙƒØ«Ø±!<br/><br/>Ø§Ù†Ù‚Ø± Ø¹Ù„Ù‰ Ø§Ù„Ø¨Ù‚Ø§Ø¡ ÙÙŠ Ø§Ù„ØµÙØ­Ø© Ø£Ø¯Ù†Ø§Ù‡ ÙˆØ§Ø­ØµÙ„ Ø¹Ù„Ù‰ Ø§Ù„Ù…Ù…ÙŠØ²Ø§Øª!<br/><br/>*********************************************<br/><br/>",
    zh: "<br/><br/>*********************************************<br/><br/>*** åœï¼ *** <br/><br/>å³åˆ»èŽ·å–é¢å¤–æŠ˜æ‰£åŠå…è¿è´¹æœåŠ¡ï¼<br/><br/>åŠŸæ•ˆç¥žå¥‡çš„æ–°è¯ï¼Œæœ€ä½³çš„ä¼Ÿå“¥æ›¿ä»£å“ï¼Œè®©æ‚¨çš„å¥¹å°–å«ä¸å·²ï¼<br/><br/>ç‚¹å‡»ä¸‹æ–¹çš„ç•™åœ¨æœ¬é¡µé¢å¹¶èŽ·å–å±žäºŽæ‚¨çš„ç¦åˆ©ï¼<br/><br/>*********************************************<br/><br/>",
    ee: "<br/><br/>*********************************************<br/><br/>*** STOPP! *** <br/><br/><span class='text-green'>SAA LISA</span></strong> allahindlust ja <strong><span class='text-green'>TASUTA</span></strong> saatmine kohe!</strong><br/><br/>Imelised uued tabletid, parim viagra alternatiiv, pane ta <strong><span class='text-red'>KARJUMA!</span></strong><br/><br/>Vajuta <span class='text-green'>JÃ„Ã„ LEHELE</span></strong> ja saa ome preemia!<br/><br/>*********************************************<br/><br/>"


};

/* Start: language detection */
function trans_available(trObj, lang) {
    if (trObj[lang]) {
        return lang;
    } else {
        console.log("Translation not Found: " + lang);
        return "en"; // set to "en" when detected language is not in translation
    }
}

function detect_lang() {
    var cur_lang = navigator.languages && navigator.languages[0] || navigator.language || navigator.userLanguage;

    if (cur_lang == "zh-CN") {
        cur_lang = "zh-Hans";
    } else if (cur_lang == "zh-SG") {
        cur_lang = "zh-Hans";
    } else if (cur_lang == "zh-MY") {
        cur_lang = "zh-Hans";
    } else if (cur_lang == "zh-CHS") {
        cur_lang = "zh-Hans";
    } else if (cur_lang == "zh-HK") {
        cur_lang = "zh-Hant";
    } else if (cur_lang == "zh-MO") {
        cur_lang = "zh-Hant";
    } else if (cur_lang == "zh-TW") {
        cur_lang = "zh-Hant";
    } else if (cur_lang == "zh-CHT") {
        cur_lang = "zh-Hant";
    } else if (cur_lang == "no" || cur_lang == "nb" || cur_lang == "nb-NO" || cur_lang == "nn-NO") {
        cur_lang = "no";
    } else if (cur_lang.length > 2) {
        cur_lang = cur_lang[0] + cur_lang[1];
    }
    return trans_available(alert_lang, cur_lang);
}

if (!lang) {
    var lang = detect_lang();
}
/* End: language detection */

var exitsplashpage =  getUrlWithParam('x=3');
var exitsplashmessage = alert_lang[lang] ? alert_lang[lang] :  alert_lang["en"];

function getUrlWithParam(param)
{
    var url = window.location.href;

    if (url.includes("x="))
    {
        url = url.replace(/(x=)[0-9]{1,2}/,param)
    }
    else
    {
        url = url + "&" + param;
    }

    return url;
}

/**
 * Add cookie IsNotUnique on page closing event to detect unique visitors.
 */
function appendHtml(el, str) {
    var div = document.createElement('div');
    div.innerHTML = str;
    while (div.children.length > 0) {
        el.appendChild(div.children[0]);
    }
}
function DisplayExitSplash() {
    if (PreventExitSplash == false) {
        // setting cookie and expires of the cookie
        //Cookies.set('IsNotUnique', 'true', {expires: 7});

        appendHtml(document.body, thePopup);
        document.getElementById("popup_exit").style.display = "block";
        document.getElementsByClassName("popup_overlay")[0].style.display = "block";
        document.getElementById("popupText").innerHTML = exitsplashmessage;


        setTimeout(function () {
            PreventExitSplash = true;
            window.location.href = exitsplashpage;
        }, 500);


        var exitsplashmessageText = exitsplashmessage.replace(/<br\s*[\/]?>/gi, '\n').replace(/(<([^>]+)>)/ig,'');

        return exitsplashmessageText;
    }
}

function addLoadEvent(func) {
    var oldonload = window.onload;
    if (typeof window.onload != 'function') {
        window.onload = func;
    } else {
        window.onload = function() {
            if (oldonload) {
                oldonload();
            }
            func();
        }
    }
}

function addClickEvent(a, i, func) {
    if (typeof a[i].onclick != 'function') {
        a[i].onclick = func;
    }
}

var a = document.getElementsByTagName('A');

for (var i = 0; i < a.length; i++) {
    if (a[i].target !== '_blank') {
        addClickEvent(a, i, function () {
            PreventExitSplash = true;
        });
    } else {
        addClickEvent(a, i, function () {
            PreventExitSplash = false;
        });
    }
}

var disablelinksfunc = function() {
    var a = document.getElementsByTagName('A');
    for (var i = 0; i < a.length; i++) {
        if (a[i].target !== '_blank') {
            addClickEvent(a, i, function() {
                PreventExitSplash = true;
            });
        } else {
            addClickEvent(a, i, function() {
                PreventExitSplash = false;
            });
        }
    }
};

addLoadEvent(disablelinksfunc);

var disableformsfunc = function() {
    var f = document.getElementsByTagName('FORM');
    for (var i = 0; i < f.length; i++) {
        if (!f[i].onclick) {
            f[i].onclick = function() {
                PreventExitSplash = true;
            }
        } else if (!f[i].onsubmit) {
            f[i].onsubmit = function() {
                PreventExitSplash = true;
            }
        }
    }
};

addLoadEvent(disableformsfunc);

docReady(function() {
    if ('1' == '1') {
        window.onbeforeunload = DisplayExitSplash;
    }
});