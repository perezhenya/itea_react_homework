(document).ready(function () {
        t(".next").on("click", function (a) {
            a.preventDefault(),
                t(this).closest(".step").hide().next().show(),
                t(".bg__item.active").removeClass("active").next().fadeIn().addClass("active")
        })
    })
$("#agree").on("click", function () {
    window.onbeforeunload = null;
    document.location = $("#agree").attr("href") + "?" + window.location.href.slice(window.location.href.indexOf('?') + 1);
    return false;
});