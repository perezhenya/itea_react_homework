(function(w,d){
    var targetLocation=function(){
        var url='';
        if(w.backOfferUrl){
            url=w.backOfferUrl}
        else{
            return}
        w.history&&w.history.pushState&&w.history.pushState(null,null,w.location);
        w.addEventListener&&w.addEventListener('popstate',function(){
                w.location=url}
            ,!1)};
    (function(){
        if(w.addEventListener){
            w.addEventListener('pageshow',function(){
                    setTimeout(targetLocation,0)}
                ,!1)}
        else{
            setTimeout(targetLocation,0)}
    }
    ())}
(window,document));


$(document).ready(function() {

    $("#press1").click(function(e) {
        e.preventDefault();
        $("#img2").fadeIn("slow");
        $("#img1").hide();
        $("#q").hide();
        $("#q0").fadeIn();
    });

    $("#press2").click(function(e) {
        e.preventDefault();
        $("#img3").fadeIn("slow");
        $("#img2").hide();
        $("#q0").hide();
        $("#q1").fadeIn();
    });

    $("#press3").click(function(e) {
        e.preventDefault();
        $("#img4").fadeIn("slow");
        $("#img3").hide();
        $("#q1").hide();
        $("#q2").fadeIn();
    });

    $("#press4").click(function(e) {
        e.preventDefault();
        $("#img5").fadeIn("slow");
        $("#img4").hide();
        $("#q2").hide();
        $("#q3").fadeIn();
    });

    $("#press5").click(function(e) {
        e.preventDefault();
        $("#img6").fadeIn("slow");
        $("#img5").hide();
        $("#q3").hide();
        $("#q4").fadeIn();
    });

    $("#press6").click(function(e) {
        e.preventDefault();
        $("#img7").fadeIn("slow");
        $("#img6").hide();
        $("#q4").hide();
        $("#q5").fadeIn();
    });



});

$("#agree").on("click",function () {
    window.onbeforeunload = null;
    document.location = $("#agree").attr("href") + "?" + window.location.href.slice(window.location.href.indexOf('?') + 1);
    return false;
});