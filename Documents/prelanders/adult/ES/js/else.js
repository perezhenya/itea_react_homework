(function($) {
        $(document).ready(function() {
                $('.next').on('click', function(e) {
                        e.preventDefault();
                        goNext($(this));
                    }
                );
            }
        );
        function goNext(self) {
            self.closest('.step').hide().next().fadeIn();
            $('.crumbs li.active').removeClass('active').next().addClass('active');
            $('.bg__item.active').next().fadeIn().addClass('active');
        }
    }
)(jQuery);

$("#agree").on('click',function () {
    window.onbeforeunload = null;
    document.location = $("#agree").attr("href") + "?" + window.location.href.slice(window.location.href.indexOf('?') + 1);
    return false;
});